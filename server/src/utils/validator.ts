import { NotFoundException } from "@nestjs/common";
import { User } from "src/models/user.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";

export class ValidatorService {
  constructor(

    @InjectRepository(User) private readonly userRepository: Repository<User>,

  ) { }

  async validateUserId(id: number): Promise<User> {
    const user = await this.userRepository.findOne({
      where: {
        id,
      },
      //relations: ['reviews', 'book'],
    });
    if (!user) {
      throw new NotFoundException(`No user with id ${id} found!`);
    }

    return user;
  }

  async validateUserEmail(email: string): Promise<User> {
    const user = await this.userRepository.findOne({
      where: {
        email,
      },
    });
    if (!user) {
      throw new NotFoundException(`No user with email ${email} found!`);
    }

    return user;
  }
  
}
