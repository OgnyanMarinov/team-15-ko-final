export const passwordGenerator = (passwordLength: number): string => {

  let result = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
  const numbers = '0123456789'
  const charactersLength = characters.length;
  const numbersLength = numbers.length;

  for (let i = 0; i < passwordLength; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength)) + numbers.charAt(Math.floor(Math.random() * numbersLength));
  }

  return result;
};
