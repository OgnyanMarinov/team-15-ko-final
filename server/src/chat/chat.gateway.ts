import {
  SubscribeMessage,
  WebSocketGateway,
  OnGatewayInit,
  WebSocketServer,
  OnGatewayConnection,
  OnGatewayDisconnect,
} from '@nestjs/websockets';
import { Logger, Inject } from '@nestjs/common';
import { Socket, Server } from 'socket.io';
import { ChatService } from './chat.service';
import {ChatMessageDTO} from '../dto/chatMessage.dto';
import {UserRoomDTO} from '../dto/userRoom.dto';

@WebSocketGateway()
export class ChatGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  @Inject()
  chatService: ChatService;
  @WebSocketServer() server: Server;
  private logger: Logger = new Logger('ChatGateway');

  @SubscribeMessage('msgToServer')
  handleMessage(
    client: Socket,
    message: ChatMessageDTO,
  ): void {
    this.server.to(message.room).emit('msgToClient', message);
    this.logger.log(`msgToClient: ${message}`);
  }
  @SubscribeMessage('joinRoom')
  async handleRoomJoin(
    client: Socket,
    userRoom: UserRoomDTO,
  ): Promise<void> {
    await this.chatService.userJoin(userRoom);
    client.join(userRoom.room);
    const participants = await this.chatService.getRoomUsers(userRoom.room);
    this.server.to(userRoom.room).emit('joinedRoom', participants);

    this.logger.log(`Client joined ${userRoom.room}`);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  afterInit(server: Server): void {
    this.logger.log('Init');
  }

  async handleDisconnect(client: Socket): Promise<void> {
    const userRoomList = await this.chatService.userClose(client.id);

    const iterate = async item => {
      await this.chatService.userLeave(item.userId, item.room);
      client.leave(item.room);
      const participants = await this.chatService.getRoomUsers(item.room);
      this.server.to(item.room).emit('leftRoom', participants);
      client.emit('leftRoom', participants);
    };

    userRoomList.forEach(iterate);
    this.logger.log(`Client disconnected: ${client.id}`);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  handleConnection(client: Socket, ...args: any[]): void {
    this.logger.log(`Client connected: ${client.id}`);
  }
  @SubscribeMessage('leaveRoom')
  async handleRoomLeave(
    client: Socket,
    userRoom: UserRoomDTO,
  ): Promise<void> {
    await this.chatService.userLeave(userRoom.userId, userRoom.room);
    client.leave(userRoom.room);
    const participants = await this.chatService.getRoomUsers(userRoom.room);
    this.server.to(userRoom.room).emit('leftRoom', participants);
    client.emit('leftRoom', participants);
    this.logger.log(`Client left ${userRoom.room}`);
  }
}
