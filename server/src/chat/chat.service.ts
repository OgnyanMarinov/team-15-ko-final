import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Chat } from 'src/models/chat.entity';
import { Repository } from 'typeorm';
import {UserRoomDTO} from '../dto/userRoom.dto';

@Injectable()
export class ChatService {
  constructor(
    @InjectRepository(Chat) private readonly chatRepository: Repository<Chat>,
  ) {}

  async userJoin(userRoom: UserRoomDTO): Promise<void> {
    await this.chatRepository.save(userRoom);
  }
  async getCurrentUser(userId: string): Promise<UserRoomDTO> {
    const user = await this.chatRepository.findOne({
      where: {
        userId: userId,
      },
    });

    return user;
  }
  async userLeave(userId: string, room:string): Promise<void> {
    const user = await this.chatRepository.findOne({
      where: {
        userId: userId,
        room: room,
      },
    });

    await this.chatRepository.remove(user);
  }
  async userClose(userId: string): Promise<UserRoomDTO[]> {
    const userRooms = await this.chatRepository.find({
      where: {
        userId: userId,
      },
    });

    return userRooms;
  }
  async getRoomUsers(room: string): Promise<any> {
    const participants = await this.chatRepository.find({
      select: ["username", "userId"],
      where: {
        room: room,
      },
    });
    return participants;
  }
}
