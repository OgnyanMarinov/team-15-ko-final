import { Module } from '@nestjs/common';
import { ServicesModule } from 'src/services/services.module';
import { AuthController } from './auth.controller';
import { UsersController } from './users.controller';
import { MailController } from './mail.controller';
import { BoardController } from './board.controller';
import { SearchController } from './search.controller';

@Module({
    imports: [ServicesModule],
    controllers: [UsersController, AuthController, MailController, BoardController, SearchController],
})
export class ControllersModule {}
