import {
  Controller,
  ValidationPipe,
  Body,
  Post,
  Param,
  ParseIntPipe,
} from '@nestjs/common';
import { MailService } from 'src/services/mail.service';
import { MailDTO } from 'src/dto/mail/mail.dto';

@Controller('mail')
export class MailController {
  constructor(private readonly appService: MailService) { }

  @Post('reset')
  sendMail(@Body(new ValidationPipe({ transform: true })) email: MailDTO): any {
    return this.appService.example(email);
  }
  @Post('invite/:boardId')
  async sendInvite(
    @Param('boardId', ParseIntPipe) boardId: number,
    @Body(new ValidationPipe({ transform: true })) email: MailDTO,
  ): Promise<any> {

    return this.appService.inviteLink(boardId, email);
  }
}
