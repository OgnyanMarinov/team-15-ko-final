import { Controller, Post, Body, ValidationPipe, Put, Param, Delete, UseGuards, ParseIntPipe, Get } from '@nestjs/common';
import { UsersService } from 'src/services/users.service';
import { CreateUserDTO } from 'src/dto/user/create-user.dto';
import { UserDTO } from 'src/dto/user/user.dto';
import { UserBanDTO } from 'src/dto/user/user-ban.dto';
import { ResponseMessageDTO } from 'src/dto/responseMessage.dto';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from 'src/auth/roles.guard';
import { UserRole } from 'src/common/user-role.enum';
import { BlacklistGuard } from 'src/auth/blacklist.guard';
import { UserProfileDTO } from 'src/dto/user/user-profile.dto';
import { UserId } from 'src/auth/user-id.decorator';
import { UserDeleteBanDTO } from 'src/dto/user/user-delete-ban.dto';
import { UserDeleteFalseDTO } from 'src/dto/user/user-delete-false.dto';
import { UserChangeRoleDTO } from 'src/dto/user/user-change-role.dto';
import { BannedDTO } from 'src/dto/user/banned-user.dto';

@Controller('users')
export class UsersController {

    constructor(
        private readonly usersService: UsersService,
    ) { }

    // User registration
    @Post()
    async create(@Body(new ValidationPipe({ whitelist: true })) userData: CreateUserDTO,
    ): Promise<UserDTO> {

        return await this.usersService.createUser(userData);
    }

    // Delete user
    @Delete(':id')
    @UseGuards(AuthGuard('jwt'),
        BlacklistGuard,
        new RolesGuard(UserRole.Admin, UserRole.Basic))
    async deleteUser(@Param('id', ParseIntPipe) userId: number): Promise<ResponseMessageDTO> {
        await this.usersService.deleteUser(userId);

        return { msg: 'User successfully deleted!' }
    }

    // Change isDeleted status to FALSE
    @Put('delete/:id')
    @UseGuards(AuthGuard('jwt'),
        BlacklistGuard,
        new RolesGuard(UserRole.Admin))
    async deleteStatusFalse(
        @Param('id', ParseIntPipe) userId: number,
        @Body(new ValidationPipe({ transform: true })) deleteStatus: UserDeleteFalseDTO
    ): Promise<UserDeleteBanDTO> {
        
        return await this.usersService.changeDeleteStatus(userId, deleteStatus);
    }

    // Change userRole Admin/Basic
    @Put('promote/:id')
    @UseGuards(AuthGuard('jwt'),
        BlacklistGuard,
        new RolesGuard(UserRole.Admin))
    async changeUserRole(
        @Param('id', ParseIntPipe) userId: number,
        @Body(new ValidationPipe({ transform: true })) changeRole: UserChangeRoleDTO
    ): Promise<UserDeleteBanDTO> {

        return await this.usersService.changeUserRole(userId, changeRole);
    }

    // Ban/unban user
    @Put(':id')
    @UseGuards(AuthGuard('jwt'),
        BlacklistGuard,
        new RolesGuard(UserRole.Admin))
    async changeUsersBanStatus(
        @Param('id', ParseIntPipe) userId: number,
        @Body(new ValidationPipe({ whitelist: true })) banInfo: UserBanDTO
    ): Promise<BannedDTO> {
       
        return await this.usersService.changeUsersBanStatus(userId, banInfo);
    }

    // getting the information for the profile page
    @Get('profile')
    @UseGuards(AuthGuard('jwt'),
        BlacklistGuard,
        new RolesGuard(UserRole.Admin, UserRole.Basic))
    async retrieveProfileInfo(@UserId() loggedUserId: number): Promise<UserProfileDTO> {

        return await this.usersService.userProfileInfo(loggedUserId);
    }

    // Retrieve info of the delete and ban status by ID
    @Get(':id')
    @UseGuards(AuthGuard('jwt'),
        BlacklistGuard,
        new RolesGuard(UserRole.Admin))
    async userDeleteBanStatus(@Param('id', ParseIntPipe) userId: number): Promise<UserDeleteBanDTO> {

        return await this.usersService.userDeleteBanStatus(userId);
    }

    // changing first, last, display name and password
    @Post('profile/edit-info')
    @UseGuards(AuthGuard('jwt'),
        BlacklistGuard,
        new RolesGuard(UserRole.Admin, UserRole.Basic))
    async editProfileInfo(
        @Body(new ValidationPipe({ transform: true })) userData: UserProfileDTO,
        @UserId() loggedUserId: number,
    ): Promise<UserProfileDTO> {

        return await this.usersService.editProfileInfo(userData, loggedUserId);
    }

    @Post('profile/edit-password')
    @UseGuards(AuthGuard('jwt'),
        BlacklistGuard,
        new RolesGuard(UserRole.Admin, UserRole.Basic))
    async editProfilePassword(
        @Body(new ValidationPipe({ transform: true })) userData: UserProfileDTO,
        @UserId() loggedUserId: number,
    ): Promise<UserProfileDTO> {

        return await this.usersService.editProfilePassword(userData, loggedUserId);
    }
}