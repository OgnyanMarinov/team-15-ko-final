import { Controller, Post, Body, Delete, Req, ValidationPipe } from '@nestjs/common';
import { AuthService } from 'src/services/auth.service';
import { UserLoginDTO } from 'src/dto/user/user-login.dto';
import { Request } from 'express';

@Controller('session')
export class AuthController {

  constructor(
    private readonly authService: AuthService,
  ) { }

  @Post()
  async login(
    @Body(new ValidationPipe({ whitelist: true}))  user: UserLoginDTO
  ): Promise<{ token: string }> { // In auth.service is ANY>>
    
    return await this.authService.login(user);
  }

  @Delete()
  async logout(
    @Req() request: Request
  ): Promise<{ message: string }> {

    const token = request.headers.authorization;
    await this.authService.blacklist(token?.slice(7));

    return {
      message: 'You have been successfully logged out!',
    };
  }
}
