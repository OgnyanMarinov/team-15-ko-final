import { Controller, Get, Query } from '@nestjs/common';
import { BoardPublicPrivateDTO } from 'src/dto/board/board-public-private.dto';
import { SearchService } from 'src/services/search.service';

@Controller('search')
export class SearchController {
  constructor(
    private readonly searchService: SearchService,
  ) { };

  @Get()
  async findBoards(@Query() query: { keyword: string }): Promise<BoardPublicPrivateDTO[]> {

    return await this.searchService.findPublicBoards(query.keyword || '');
  }
}

