import { Controller, Post, Body, ValidationPipe, Get, UseGuards, Param, ParseIntPipe } from '@nestjs/common';
import { BoardService } from 'src/services/board.service';
import { BoardCreateDTO } from 'src/dto/board/board-create.dto';
import { BoardDTO } from 'src/dto/board/board.dto';
import { AuthGuard } from '@nestjs/passport';
import { BlacklistGuard } from 'src/auth/blacklist.guard';
import { RolesGuard } from 'src/auth/roles.guard';
import { UserRole } from 'src/common/user-role.enum';
import { UserId } from 'src/auth/user-id.decorator';
import { BoardLoadDTO } from 'src/dto/board/board-load.dto';
import { BoardSaveDTO } from 'src/dto/board/board-save.dto';
import { BoardPublicPrivateDTO } from 'src/dto/board/board-public-private.dto';
import { BoardDeleteDTO } from 'src/dto/board/board-delete.dto';
import { BoardRenameDTO } from 'src/dto/board/board-rename.dto';
import { BoardChangeStatusDTO } from 'src/dto/board/board-change-status.dto';

@Controller('board')
export class BoardController {
  constructor(private readonly boardService: BoardService) { }

  @Post('create')
  @UseGuards(
    AuthGuard('jwt'),
    BlacklistGuard,
    new RolesGuard(UserRole.Admin, UserRole.Basic),
  )
  async create(
    @Body(new ValidationPipe({ whitelist: false })) boardData: BoardCreateDTO,
    @UserId() loggedUserId: number,
  ): Promise<BoardDTO> {
    
    return await this.boardService.createBoard(boardData, loggedUserId);
  }

  @Post('save')
  @UseGuards(
    AuthGuard('jwt'),
    BlacklistGuard,
    new RolesGuard(UserRole.Admin, UserRole.Basic),
  )
  async save(
    @Body(new ValidationPipe({ whitelist: false })) boardData: BoardSaveDTO,
    // @UserId() loggedUserId: number,
  ): Promise<BoardDTO> {
    
    return await this.boardService.saveBoard(boardData); //, loggedUserId);
  }

  @Post('load')
  @UseGuards(
    AuthGuard('jwt'),
    BlacklistGuard,
    new RolesGuard(UserRole.Admin, UserRole.Basic),
  )
  async load(
    @Body(new ValidationPipe({ whitelist: false })) boardId: BoardLoadDTO,
    @UserId() loggedUserId: number,
  ): Promise<BoardDTO> {
    
    return await this.boardService.loadBoard(boardId, loggedUserId);
  }

  @Get('public')
  @UseGuards(
    AuthGuard('jwt'),
    BlacklistGuard,
    new RolesGuard(UserRole.Admin, UserRole.Basic),
  )
  async retrievePublicBoards(
    @UserId() loggedUserId: number,
  ): Promise<BoardPublicPrivateDTO[]> {
    return await this.boardService.publicBoards(loggedUserId);
  }

  @Get('all-users-public')
  @UseGuards(
    AuthGuard('jwt'),
    BlacklistGuard,
    new RolesGuard(UserRole.Admin, UserRole.Basic),
  )
  async retrieveAllUsersPublicBoards(): Promise<BoardPublicPrivateDTO[]> {
    return await this.boardService.allUsersPublicBoards();
  }

  @Get('private')
  @UseGuards(
    AuthGuard('jwt'),
    BlacklistGuard,
    new RolesGuard(UserRole.Admin, UserRole.Basic),
  )
  async retrievePrivateBoards(
    @UserId() loggedUserId: number,
  ): Promise<BoardPublicPrivateDTO[]> {
    return await this.boardService.privateBoards(loggedUserId);
  }

  @Post('delete')
  @UseGuards(
    AuthGuard('jwt'),
    BlacklistGuard,
    new RolesGuard(UserRole.Admin, UserRole.Basic),
  )
  async delete(
    @Body(new ValidationPipe({ whitelist: false })) boardData: BoardDeleteDTO,
    @UserId() loggedUserId: number,
  ): Promise<BoardDTO> {
    return await this.boardService.deleteBoard(boardData, loggedUserId);
  }

  @Post('rename')
  @UseGuards(
    AuthGuard('jwt'),
    BlacklistGuard,
    new RolesGuard(UserRole.Admin, UserRole.Basic),
  )
  async rename(
    @Body(new ValidationPipe({ whitelist: false })) boardData: BoardRenameDTO,
    @UserId() loggedUserId: number,
  ): Promise<BoardDTO> {
    return await this.boardService.renameBoard(boardData, loggedUserId);
  }

  @Post('change-status')
  @UseGuards(
    AuthGuard('jwt'),
    BlacklistGuard,
    new RolesGuard(UserRole.Admin, UserRole.Basic),
  )
  async changeStatus(
    @Body(new ValidationPipe({ whitelist: false }))
    boardData: BoardChangeStatusDTO,
    @UserId() loggedUserId: number,
  ): Promise<BoardDTO> {
    return await this.boardService.changeBoardStatus(boardData, loggedUserId);
  }

  @Get(':id')
  @UseGuards(
    AuthGuard('jwt'),
    BlacklistGuard,
    new RolesGuard(UserRole.Admin, UserRole.Basic),
  )
  async retrieveBoardById(
    @Param('id', ParseIntPipe) boardId: number,
    @UserId() loggedUserId: number,
  ): Promise<BoardDTO> {
    return await this.boardService.loadBoardById(boardId, loggedUserId);
  }

}
