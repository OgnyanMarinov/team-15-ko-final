import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/models/user.entity';
import { Repository } from 'typeorm';
import { MailDTO } from 'src/dto/mail/mail.dto';
import { ValidatorService } from 'src/utils/validator';
import * as bcrypt from 'bcrypt';
import { passwordGenerator } from 'src/utils/passwordGenerator';
import { Access } from 'src/models/access.entity';

@Injectable()
export class MailService {
  constructor(
    private readonly mailerService: MailerService,
    private readonly validator: ValidatorService,
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
    @InjectRepository(Access)
    private readonly AccessRepository: Repository<Access>,
  ) { }

  async example(email: MailDTO): Promise<any> {
    const user = await this.validator.validateUserEmail(email.email);
    const newPassword = passwordGenerator(5);

    user.password = await bcrypt.hash(newPassword, 10);

    await this.usersRepository.save(user);

    this.mailerService
      .sendMail({
        to: email.email, // List of receivers email address
        from: 'ko.whiteboard.platform@gmail.com', // Senders email address
        subject: 'Your password was reset', // Subject line
        text: 'welcome', // plaintext body
        html: `<b>This is an automated message! Please do not reply!</b><br /><br />
        Hello <b><i>${user.username}</b></i>,<br /><br />
        We wanted to let you know that your password for <b>KO's Whiteboard Platform</b> was reset.<br />
        Please use this password <b>${newPassword}</b> on your next log in. You can continue using this password
        or change it from your <b>Profile page</b>.<br /><br />
        We will never ask for your password, and we strongly discourage you from sharing it with anyone.<br /><br />
        <b><i>KO's Whiteboard Platform Team</b></i>`, // HTML body content
      })
      .then(success => {
        console.log(success);
      })
      .catch(err => {
        console.log(err);
      });
  }
  async inviteLink(boardId: number, email: MailDTO): Promise<any> {
    const user = await this.validator.validateUserEmail(email.email);
    const boardAccess = {
      userEmail: email.email,
      boardNumber: boardId,
    };
    const foundAccess = await this.AccessRepository.findOne(boardAccess);
    if (foundAccess) {
      await this.AccessRepository.save({ ...foundAccess });
    } else {
      await this.AccessRepository.save(boardAccess);
      this.mailerService
        .sendMail({
          to: email.email, // List of receivers email address
          from: 'ko.whiteboard.platform@gmail.com', // Senders email address
          subject: 'You have been invited', // Subject line
          text: 'welcome', // plaintext body
          html: `<b>This is an automated message! Please do not reply!</b><br /><br />
      Hello <b><i>${user.username}</b></i>,<br /><br />
      We wanted to let you know that you have been invited to a board.<br />
      Please follow the link <a href="http://localhost:4000/boards/${boardId}">click</a> to start collaborating with other team members.<br /><br />
      <b><i>KO's Whiteboard Platform Team</b></i>`, // HTML body content
        })
        .then(success => {
          console.log(success);
        })
        .catch(err => {
          console.log(err);
        });
    }
  }
}
