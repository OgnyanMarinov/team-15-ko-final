import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BoardPublicPrivateDTO } from 'src/dto/board/board-public-private.dto';
import { Board } from 'src/models/board.entity';
import { Like, Repository } from 'typeorm';
import { TransformService } from './transform.service';

@Injectable()
export class SearchService {

  constructor(
    @InjectRepository(Board)
    private readonly boardRepository: Repository<Board>,
    private readonly transformer: TransformService,
  ) { };

  async findPublicBoards(keyword: string): Promise<BoardPublicPrivateDTO[]> {

    const foundBoards = await this.boardRepository.find({
      where: {
        boardName: Like(`%${keyword}%`),
        isPrivate: false,
      },
    });

    return this.transformer.toPublicPrivateBoard(foundBoards);
  }
}
