import { Injectable, BadRequestException, NotAcceptableException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/models/user.entity';
import { Repository } from 'typeorm';
import { CreateUserDTO } from 'src/dto/user/create-user.dto';
import { UserDTO } from 'src/dto/user/user.dto';
import * as bcrypt from 'bcrypt';
import { TransformService } from './transform.service';
import { UserBanDTO } from 'src/dto/user/user-ban.dto';
import { UserProfileDTO } from 'src/dto/user/user-profile.dto';
import { UserDeleteBanDTO } from 'src/dto/user/user-delete-ban.dto';
import { UserDeleteFalseDTO } from 'src/dto/user/user-delete-false.dto';
import { UserChangeRoleDTO } from 'src/dto/user/user-change-role.dto';
import { BannedDTO } from 'src/dto/user/banned-user.dto';

@Injectable()
export class UsersService {

    constructor(
        private readonly transformer: TransformService,
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
    ) { }

    async createUser(userData: CreateUserDTO): Promise<UserDTO> {
        // validate that no such username exists
        const searchDuplicateUsername = await this.usersRepository.findOne({
            where: {
                username: userData.username,
            }
        });
        if (searchDuplicateUsername) {
            throw new BadRequestException(`Username \'${userData.username}\' is already taken!`)
        }

        // validate that no such email exists
        const searchDuplicateEmail = await this.usersRepository.findOne({
            where: {
                email: userData.email,
            }
        });
        if (searchDuplicateEmail) {
            throw new BadRequestException(`User with \'${userData.email}\' is already registered!`)
        }

        const user = this.usersRepository.create(userData);

        user.displayName = user.username;

        user.password = await bcrypt.hash(user.password, 10);

        const created = await this.usersRepository.save(user);

        return this.transformer.toUserDTO(created);
    }

    async deleteUser(id: number): Promise<void> {
        const foundUser: User = await this.findUserEntityById(id);

        await this.usersRepository.save({
            ...foundUser,
            isDeleted: true,
        });
    }

    async findUserEntityById(id: number): Promise<User> {
        const foundUser: User = await this.usersRepository.findOne({
            where: {
                id,
                isDeleted: false,
            }
        });

        if (!foundUser) {
            throw new BadRequestException('User does not exist');
        }

        return foundUser;
    }

    async changeUsersBanStatus(id: number, banInfo: UserBanDTO): Promise<BannedDTO> {
        
        const foundUser: User = await this.findUserEntityById(id);

        if (foundUser.isBanned) {
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            const statusUnban = `${foundUser.username} has been unbanned!`;

            foundUser.isBanned = false;
            foundUser.descriptionBan = 'Unbanned!'
            foundUser.periodBanned = null;
            foundUser.userRole = 2; // even if he was Admin before, he will start from the bottom after the ban!

            await this.usersRepository.save(foundUser);

            return this.transformer.toUserBanDTO(foundUser);

        } else {
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            const statusBan = `${foundUser.username} has been banned`;

            const period = banInfo.banTime;
            const periodBanned = new Date(Date.now() + (period * 1000 * 60 * 60 * 24)); //Period is day
            foundUser.isBanned = true;
            foundUser.descriptionBan = banInfo.banInfo;
            foundUser.periodBanned = periodBanned;
            foundUser.userRole = 3;

            await this.usersRepository.save(foundUser);

            return this.transformer.toUserBanDTO(foundUser);
        }
    }

    async userProfileInfo(id: number): Promise<UserProfileDTO> {
        const foundUser: User = await this.findUserEntityById(id);

        return this.transformer.toUserProfileNoPasswordDTO(foundUser);
    }

    async editProfileInfo(userData: UserProfileDTO, loggedUserId: number): Promise<UserProfileDTO> {

        if (loggedUserId !== userData.id) {
            throw new NotAcceptableException(`ID's does not match!`)
        }
        const foundUser: User = await this.findUserEntityById(loggedUserId);

        foundUser.firstName = userData.firstName;
        foundUser.lastName = userData.lastName;
        foundUser.displayName = userData.displayName;

        const updatedProfile = await this.usersRepository.save(foundUser);

        return this.transformer.toUserProfileDTO(updatedProfile);
    }

    async editProfilePassword(userData: UserProfileDTO, loggedUserId: number): Promise<UserProfileDTO> {

        if (loggedUserId !== userData.id) {
            throw new NotAcceptableException(`ID's does not match!`)
        }
        const foundUser: User = await this.findUserEntityById(loggedUserId);

        const match = await bcrypt.compare(userData.password, foundUser.password);

        if (match) {
            foundUser.password = userData.newPassword;
            foundUser.password = await bcrypt.hash(foundUser.password, 10);
        } else {
            throw new NotAcceptableException(`Current password does not match!`);
        }

        const updatedProfile = await this.usersRepository.save(foundUser);

        return this.transformer.toUserProfileDTO(updatedProfile);
    }

    async userDeleteBanStatus(userId: number): Promise<UserDeleteBanDTO> {
        const foundUser: User = await this.usersRepository.findOne({
            where: {
                id: userId,
                // isDeleted: false,
            }
        });

        return this.transformer.toUserDeleteBanStatus(foundUser);
    }

    async changeDeleteStatus(userId: number, deleteStatus: UserDeleteFalseDTO): Promise<UserDeleteBanDTO> {
        const foundUser: User = await this.usersRepository.findOne({
            where: {
                id: userId,
                // isDeleted: false,
            }
        });

        foundUser.isDeleted = deleteStatus.isDeleted;

        const updatedUser = await this.usersRepository.save(foundUser);

        return this.transformer.toUserDeleteBanStatus(updatedUser);
    }

    async changeUserRole(userId: number, changeRole: UserChangeRoleDTO): Promise<UserDeleteBanDTO> {
        const foundUser: User = await this.usersRepository.findOne({
            where: {
                id: userId,
                isDeleted: false,
            }
        });
        foundUser.userRole = changeRole.userRole;

        const updatedUser = await this.usersRepository.save(foundUser);

        return this.transformer.toUserDeleteBanStatus(updatedUser);
    }
}
