/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Injectable, UnauthorizedException, ConflictException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/models/user.entity';
import { Repository } from 'typeorm';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { UserLoginDTO } from 'src/dto/user/user-login.dto';
import { JWTPayload } from 'src/common/jwt-payload';
import { UserRole } from 'src/common/user-role.enum';
import { Token } from 'src/models/token.entity';

@Injectable()
export class AuthService {

  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(Token) private readonly tokenRepository: Repository<Token>,
    private readonly jwtService: JwtService,
  ) { }

  async findUserByName(usernameOrEmail: string) {

    const searchUsername = await this.userRepository.findOne({
      where: {
        username: usernameOrEmail,
        isDeleted: false,
      }
    });

    return searchUsername
      ? searchUsername
      : await this.userRepository.findOne({
        where: {
          email: usernameOrEmail,
          isDeleted: false,
        }
      });
  }

  async validateUser(usernameOrEmail: string, password: string) {
    const user = await this.findUserByName(usernameOrEmail);
    if (!user) {
      return null;
    }
    const isUserValidated = await bcrypt.compare(password, user.password);
    return isUserValidated
      ? user
      : null;
  }

  async login(loginUser: UserLoginDTO): Promise<any> {
    const user = await this.validateUser(loginUser.usernameOrEmail, loginUser.password);

    if (!user) {
      throw new UnauthorizedException('Wrong credentials!');
    }
    if (user.isBanned) {
      throw new UnauthorizedException(
        'Account has been banned. Please contact an administrator.');
    }
    if (user.isDeleted) {
      throw new UnauthorizedException(
        `This account has been deleted! Please contact an administrator.`);
    }


    const payload: JWTPayload = {
      id: user.id,
      username: user.username,
      role: UserRole[user.userRole],
    }

    const token = await this.jwtService.signAsync(payload);

    // this will be returned to the client on successful login
    return {
      token,
      message: "You have just logged in!",
      userDetails: payload,
    };
  }

  async blacklist(tokenIncome: string) {
    const checkToken = await this.tokenRepository.findOne({
      where: { token: tokenIncome }
    });

    if (!!checkToken) {
      throw new ConflictException('You have already logged out! FYI: Duplicate token!')
    }

    const tokenEntity = this.tokenRepository.create();
    tokenEntity.token = tokenIncome;

    await this.tokenRepository.save(tokenEntity)
  }

  async isBlacklisted(token: string): Promise<boolean> {
    return Boolean(await this.tokenRepository.findOne({
      where: {
        token,
      }
    }));
  }
}
