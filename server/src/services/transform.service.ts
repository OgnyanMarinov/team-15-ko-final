import { User } from "src/models/user.entity";
import { UserDTO } from "src/dto/user/user.dto";
import { UserProfileDTO } from "src/dto/user/user-profile.dto";
import { UserDeleteBanDTO } from "src/dto/user/user-delete-ban.dto";
import { BannedDTO } from "src/dto/user/banned-user.dto";
import { Board } from "src/models/board.entity";
import { BoardDTO } from "src/dto/board/board.dto";
import { BoardPublicPrivateDTO } from "src/dto/board/board-public-private.dto";

export class TransformService {

    toUserDTO(user: User): UserDTO {
        return {
            id: user.id,
            username: user.username,
            email: user.email,
            displayName: user.displayName,
            userRole: user.userRole,
        }
    }

    toUserProfileDTO(user: User): UserProfileDTO {
        return {
            id: user.id,
            username: user.username,
            email: user.email,
            firstName: user.firstName,
            lastName: user.lastName,
            displayName: user.displayName,
            userRole: user.userRole,
            isDeleted: user.isDeleted,
            password: user.password,
            newPassword: user.newPassword,
        }
    }

    toUserProfileNoPasswordDTO(user: User): UserProfileDTO {
        return {
            id: user.id,
            username: user.username,
            email: user.email,
            firstName: user.firstName,
            lastName: user.lastName,
            displayName: user.displayName,
            userRole: user.userRole,
            isDeleted: user.isDeleted,
            password: null,
            newPassword: null,
        }
    }

    toUserBanDTO(user: User): BannedDTO {
        return {
            id: user.id,
            username: user.username,
            userRole: user.userRole,

        }
    }

    toUserDeleteBanStatus(user: User): UserDeleteBanDTO {
        return {
            id: user.id,
            username: user.username,
            userRole: user.userRole,
            isDeleted: user.isDeleted,
            isBanned: user.isBanned,
            descriptionBan: user.descriptionBan,
            periodBanned: user.periodBanned,
        }
    }

    toBoardDTO(board: Board): BoardDTO {
        return {
            id: board.id,
            boardName: board.boardName,
            dateCreated: board.dateCreated,
            linesData: board.linesData,
            textsData: board.textsData,
            isPrivate: board.isPrivate,
        }
    }

    toPublicPrivateBoard(board: Board[]): BoardPublicPrivateDTO[] {
        return board.map(k => {
            return {
                id: k.id,
                boardName: k.boardName,
                dateCreated: k.dateCreated,
            }
        });
    }
}
