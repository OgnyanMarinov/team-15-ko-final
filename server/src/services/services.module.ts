import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/models/user.entity';
import { TransformService } from './transform.service';
import { ValidatorService } from 'src/utils/validator';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from 'src/constant/secret';
import { AuthService } from './auth.service';
import { JwtStrategy } from './strategy/jwt-strategy';
import { UsersService } from 'src/services/users.service';
import { Token } from 'src/models/token.entity';
import { TasksService } from './tasks.service';
import { MailerModule } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { MailService } from './mail.service';
import { mailCredentials } from 'src/constant/mailCredentials';
import { Chat } from 'src/models/chat.entity';
import { ChatService } from 'src/chat/chat.service';
import { ChatGateway } from 'src/chat/chat.gateway';
import { BoardService } from './board.service';
import { Board } from 'src/models/board.entity';
import {Access} from 'src/models/access.entity'
import { SearchService } from './search.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Token, Board, Chat, Access]),
    PassportModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: {
        expiresIn: '7d', // change to 24h!!
      },
    }),
    MailerModule.forRoot({
      transport: {
        host: 'smtp.gmail.com',
        port: 465,
        tls: {
          ciphers: 'SSLv3',
        },
        secure: true, // true for 465, false for other ports
        auth: {
          user: mailCredentials.username,
          pass: mailCredentials.password,
        },
      },
      defaults: {
        from: '"No Reply" <ko.whiteboard.platform@gmail.com>', // outgoing email ID
      },
      template: {
        dir: process.cwd() + '/template/',
        adapter: new HandlebarsAdapter(), // or new PugAdapter() or new EjsAdapter()
        options: {
          strict: true,
        },
      },
    }),
  ],
  providers: [
    UsersService,
    TransformService,
    ValidatorService,
    AuthService,
    JwtStrategy,
    TasksService,
    MailService,
    ChatService,
    ChatGateway,
    BoardService,
    SearchService,
  ],
  exports: [UsersService, AuthService, MailService, BoardService, SearchService],
})
export class ServicesModule {}
