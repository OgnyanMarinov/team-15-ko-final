import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BoardCreateDTO } from 'src/dto/board/board-create.dto';
import { BoardDTO } from 'src/dto/board/board.dto';
import { TransformService } from './transform.service';
import { Board } from 'src/models/board.entity';
import { BoardLoadDTO } from 'src/dto/board/board-load.dto';
import { BoardSaveDTO } from 'src/dto/board/board-save.dto';
import { ValidatorService } from 'src/utils/validator';
import { BoardPublicPrivateDTO } from 'src/dto/board/board-public-private.dto';
import { BoardDeleteDTO } from 'src/dto/board/board-delete.dto';
import { BoardRenameDTO } from 'src/dto/board/board-rename.dto';
import { BoardChangeStatusDTO } from 'src/dto/board/board-change-status.dto';
import { Access } from 'src/models/access.entity';

@Injectable()
export class BoardService {
  constructor(
    private readonly transformer: TransformService,
    private readonly validator: ValidatorService,
    @InjectRepository(Board)
    private readonly boardRepository: Repository<Board>,
    @InjectRepository(Access)
    private readonly AccessRepository: Repository<Access>,
  ) { }

  async createBoard(
    boardData: BoardCreateDTO,
    loggedUserId: number,
  ): Promise<BoardDTO> {
    const foundUser = await this.validator.validateUserId(loggedUserId);
    const searchDuplicateBoard = await this.boardRepository.findOne({
      where: {
        boardName: boardData.boardName,
        user: foundUser,
      },
    });
    if (searchDuplicateBoard) {
      throw new BadRequestException(
        `You already have a board with name \'${boardData.boardName}\'!`,
      );
    }

    const board = this.boardRepository.create(boardData);
    board.user = foundUser;
    const created = await this.boardRepository.save(board);

    return this.transformer.toBoardDTO(created);
  }

  async saveBoard(boardData: BoardSaveDTO): Promise<BoardDTO> {
    //, loggedUserId: number
    // const foundUser = await this.validator.validateUserId(loggedUserId);
    const foundBoard = await this.boardRepository.findOne({
      where: {
        id: boardData.id,
        // user: foundUser,
      },
    });
    if (!foundBoard) {
      throw new BadRequestException(`Board was not found!`);
    }
    // foundBoard.linesData = boardData.linesData;
    // foundBoard.textsData = boardData.textsData;
    const saved = await this.boardRepository.save({
      ...foundBoard,
      ...boardData,
    });

    return this.transformer.toBoardDTO(saved);
  }

  async loadBoard(
    boardId: BoardLoadDTO,
    loggedUserId: number,
  ): Promise<BoardDTO> {
    const foundUser = await this.validator.validateUserId(loggedUserId);
    const foundBoard = await this.boardRepository.findOne({
      where: {
        id: boardId.id,
      },
    });
    const foundOwnBoard = await this.boardRepository.findOne({
      where: {
        id: boardId.id,
        user: foundUser,
      },
    });

    if (!foundBoard) {
      console.log('Test case 1');
      throw new BadRequestException(`Board was not found!`);
    }
    else if (!foundBoard.isPrivate && foundUser) {
      console.log('Test case 2');
      return this.transformer.toBoardDTO(foundBoard);
    }
    else if (foundBoard.isPrivate && foundOwnBoard) {
      console.log('test case 3');
      return this.transformer.toBoardDTO(foundBoard);
    }
    else if (foundBoard.isPrivate && foundUser) {
      const foundAccess = await this.AccessRepository.findOne({
        where: {
          userEmail: foundUser.email,
          boardNumber: boardId.id,
        }
      })
      if (foundAccess) {
        console.log('Test case 4');
        return this.transformer.toBoardDTO(foundBoard);

      } else if (!foundAccess) {
        console.log('Test case 5');
        throw new BadRequestException(`You don't have access to this board!`);
      }
    }

  }
  async publicBoards(loggedUserId: number): Promise<BoardPublicPrivateDTO[]> {
    const foundUser = await this.validator.validateUserId(loggedUserId);
    const foundPublicBoards = await this.boardRepository.find({
      where: {
        user: foundUser,
        isPrivate: false,
        isDeleted: false,
      },
    });

    return this.transformer.toPublicPrivateBoard(foundPublicBoards);
  }

  async allUsersPublicBoards(): Promise<BoardPublicPrivateDTO[]> {
    const foundPublicBoards = await this.boardRepository.find({
      where: {
        isPrivate: false,
        isDeleted: false,
      },
    });

    return this.transformer.toPublicPrivateBoard(foundPublicBoards);
  }
  async privateBoards(loggedUserId: number): Promise<BoardPublicPrivateDTO[]> {
    const foundUser = await this.validator.validateUserId(loggedUserId);
    const foundPrivateBoards = await this.boardRepository.find({
      where: {
        user: foundUser,
        isPrivate: true,
        isDeleted: false,
      },
    });

    return this.transformer.toPublicPrivateBoard(foundPrivateBoards);
  }

  async deleteBoard(
    boardData: BoardDeleteDTO,
    loggedUserId: number,
  ): Promise<BoardDTO> {
    const foundUser = await this.validator.validateUserId(loggedUserId);
    const foundBoard = await this.boardRepository.findOne({
      where: {
        id: boardData.id,
        user: foundUser,
      },
    });
    if (!foundBoard) {
      throw new BadRequestException(
        `Board was not found or you are not the owner!`,
      );
    }

    const saved = await this.boardRepository.save({
      ...foundBoard,
      ...boardData,
    });

    return this.transformer.toBoardDTO(saved);
  }

  async renameBoard(
    boardData: BoardRenameDTO,
    loggedUserId: number,
  ): Promise<BoardDTO> {
    const foundUser = await this.validator.validateUserId(loggedUserId);
    const foundBoard = await this.boardRepository.findOne({
      where: {
        id: boardData.id,
        user: foundUser,
      },
    });
    if (!foundBoard) {
      throw new BadRequestException(
        `Board was not found or you are not the owner!`,
      );
    }

    const saved = await this.boardRepository.save({
      ...foundBoard,
      ...boardData,
    });

    return this.transformer.toBoardDTO(saved);
  }

  async changeBoardStatus(
    boardData: BoardChangeStatusDTO,
    loggedUserId: number,
  ): Promise<BoardDTO> {
    const foundUser = await this.validator.validateUserId(loggedUserId);
    const foundBoard = await this.boardRepository.findOne({
      where: {
        id: boardData.id,
        user: foundUser,
      },
    });
    if (!foundBoard) {
      throw new BadRequestException(
        `Board was not found or you are not the owner!`,
      );
    }

    const saved = await this.boardRepository.save({
      ...foundBoard,
      ...boardData,
    });

    return this.transformer.toBoardDTO(saved);
  }

  async loadBoardById(boardId: number, loggedUserId: number): Promise<BoardDTO> {
    const foundUser = await this.validator.validateUserId(loggedUserId);
    const foundBoard: Board = await this.boardRepository.findOne({
      where: {
        id: boardId,
        user: foundUser,
      }
    });

    if (!foundBoard) {
      throw new BadRequestException('Board does not exist!');
    }

    return this.transformer.toBoardDTO(foundBoard);
  }

}
