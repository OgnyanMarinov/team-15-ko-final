import { Injectable, Logger } from '@nestjs/common';
// import { Cron, CronExpression } from '@nestjs/schedule';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, /*LessThan*/ } from 'typeorm';
import { Token } from 'src/models/token.entity';

@Injectable()
export class TasksService {
    private readonly logger = new Logger(TasksService.name);

    constructor(
        @InjectRepository(Token) private readonly tokensRepository: Repository<Token>,
    ) { }
    
    // @Cron(CronExpression.EVERY_10_SECONDS)
    // async handleCron() {
    //     this.logger.debug('Called every 10 seconds!');

    //     await this.tokensRepository
    //         .createQueryBuilder()
    //         .delete()
    //         .from(Token)
    //         .where({ blacklistedOn: LessThan(new Date(Date.now() - 3600000))}) // 1 hour
    //         .execute()
    // }
}
