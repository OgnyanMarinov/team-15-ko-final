export class ChatMessageDTO {
  sender: string; 
  room: string; 
  message: string; 
  time: string; 
}