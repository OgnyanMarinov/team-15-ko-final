
export class BoardDeleteDTO {
  id: number;
  isDeleted: boolean;
}