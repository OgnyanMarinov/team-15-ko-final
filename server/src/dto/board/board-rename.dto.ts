
export class BoardRenameDTO {
  id: number;
  boardName: string;
}