
export class BoardChangeStatusDTO {
  id: number;
  isPrivate: boolean;
}