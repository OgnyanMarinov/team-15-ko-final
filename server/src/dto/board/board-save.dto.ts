
export class BoardSaveDTO {
  id: number;
  linesData: string;
  textsData: string;
}