
export class BoardCreateDTO {
  boardName: string;
  isPrivate: boolean;
}