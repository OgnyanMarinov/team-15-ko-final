
export class BoardPublicPrivateDTO {
  id: number;
  boardName: string;
  dateCreated: Date;
}