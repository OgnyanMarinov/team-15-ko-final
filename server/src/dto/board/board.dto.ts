
export class BoardDTO {
  id: number;
  boardName: string;
  dateCreated: Date;
  linesData: string;
  textsData: string;
  isPrivate: boolean;
}