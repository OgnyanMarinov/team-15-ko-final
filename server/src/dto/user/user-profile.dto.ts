import { UserRole } from "src/common/user-role.enum";

export class UserProfileDTO {
  id: number;
  username: string;
  email: string;
  firstName: string;
  lastName: string;
  displayName: string;
  userRole: UserRole;
  isDeleted: boolean;
  password: string;
  newPassword: string;
}
