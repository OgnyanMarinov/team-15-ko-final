import { UserRole } from "src/common/user-role.enum";

export class UserDeleteBanDTO {
  id: number;
  username: string;
  userRole: UserRole;
  isDeleted: boolean;
  isBanned: boolean;
  descriptionBan: string;
  periodBanned: Date;
}