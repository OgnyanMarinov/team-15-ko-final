import { UserRole } from "src/common/user-role.enum";
export class BannedDTO {
  id: number;
  username: string;
  
  userRole: UserRole;
  
}