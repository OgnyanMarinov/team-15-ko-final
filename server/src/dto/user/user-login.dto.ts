import { IsNotEmpty} from "class-validator";
export class UserLoginDTO {
    @IsNotEmpty()
    usernameOrEmail: string;
    @IsNotEmpty()
    password: string;
}