import { UserRole } from "src/common/user-role.enum";

export class UserChangeRoleDTO {
  userRole: UserRole;

}