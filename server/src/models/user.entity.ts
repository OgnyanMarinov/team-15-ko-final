import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { UserRole } from 'src/common/user-role.enum';
import { Board } from './board.entity';

@Entity('user')
export class User {
   
    @PrimaryGeneratedColumn()
    id: number;
    @Column({ nullable: false, length: 25 })
    username: string;
    @Column({ nullable: false })
    email: string;
    @Column({ nullable: true, length: 20 })
    firstName: string;
    @Column({ nullable: true, length: 20 })
    lastName: string;
    @Column({ nullable: true, length: 20 })
    displayName: string;
    @Column({ nullable: false })
    password: string;
    @Column({ nullable: true })
    newPassword: string;
    @Column({ type: 'enum', enum: UserRole, default: UserRole.Basic })
    userRole: UserRole;
    @Column({ default: false })
    isDeleted: boolean;
    @Column({ default: false })
    isBanned: boolean;
    @Column({ default: null })
    descriptionBan: string;
    @Column({ default: null })
    periodBanned: Date;
    @OneToMany(
        () => Board,
        board => board.user
    )
    boardsUser: Board[];
}