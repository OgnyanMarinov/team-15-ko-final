import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, CreateDateColumn } from 'typeorm';
import { User } from './user.entity';

@Entity('board')
export class Board {
   
    @PrimaryGeneratedColumn()
    id: number;
    @Column({ nullable: false, length: 40 })
    boardName: string;
    @CreateDateColumn()
    dateCreated: Date;
    @Column({ type: "text", nullable: true })
    linesData: string;
    @Column({ type: "text", nullable: true })
    textsData: string;
    @Column({ default: false })
    isDeleted: boolean;
    @Column({ default: false })
    isPrivate: boolean;

    @ManyToOne(
        () => User,
        user => user.boardsUser
    )
    user: User;
}