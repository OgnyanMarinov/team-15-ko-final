import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('chat')
export class Chat {
 
  @PrimaryGeneratedColumn()
   id: string;

  @Column({ nullable: true})
  username: string;

  @Column({ nullable: true})
  room: string;

  @Column({ nullable: true})
  userId: string;
}