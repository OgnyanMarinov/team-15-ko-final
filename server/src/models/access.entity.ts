import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('access')
export class Access {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  userEmail: string;

  @Column({ nullable: true })
  boardNumber: number;

}