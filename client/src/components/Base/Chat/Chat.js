import React, { useState, useEffect, useContext } from "react";
import io from "socket.io-client";
import { BASE_URL, TOKEN } from "../../../common/constants";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPaperPlane } from "@fortawesome/free-solid-svg-icons";
import { faComment } from "@fortawesome/free-solid-svg-icons";
import { faUser } from "@fortawesome/free-solid-svg-icons";
import AuthContext from "../../../providers/AuthContext";
import "./Chat.css";

const Chat = (Id) => {
  const [state, setState] = useState({
    sender: "",
    room: "",
    message: "",
    time: "",
  });
  const [socketId, setSocketId] = useState();
  const [chat, setChat] = useState([
    { sender: "Peter", room: "General", message: "test", time: "10:00" },
  ]);
  const [socket, setSocket] = useState({});
  const [chatRoom, setchatRoom] = useState({
    roomName: "",
    joined: false,
  });
  const [participants, setParticipants] = useState([]);
  const { user } = useContext(AuthContext);
 

  useEffect(() => {
    const socket = io(BASE_URL);
    setSocket(socket);
    socket.on("connect", () => {
      setSocketId(socket.id);
    });
    socket.on("msgToClient", (msg) => {
      setChat((chat) => [...chat, { ...msg }]);
    });
    socket.on("joinedRoom", (particip) => {
      setParticipants(particip);
    });

    socket.on("leftRoom", (particip) => {
      setParticipants(particip);
    });
  
    fetch(`${BASE_URL}/board/load`, {
      method: "POST",
      headers: {
        Authorization: TOKEN,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id: Id.Id,
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        const name = result.boardName;
        
        setchatRoom({...chatRoom, roomName: name});
      })
      .catch(error => error.message);
  }, []);
console.log(chatRoom);
  const onTextChange = (e) => {
    setState({ ...state, [e.target.name]: e.target.value });
  };
 
  const onMessageSubmit = (e) => {
    e.preventDefault();

    const sender = user.username;
    const room = chatRoom.roomName;
    const { message } = state;
    const time = new Date().toLocaleTimeString();

    socket.emit("msgToServer", { sender, room, message, time });
    setState({ message: "", sender: "", room: "", time: "" });
  };
  const toggleRoomMembership = (r) => {
    const userRoom = {
      userId: socketId,
      username: user.username,
      room: r,
    };
    const isMemberOfActiveRoom = () => {
      
      
      return chatRoom.joined;
    };
    if (isMemberOfActiveRoom()) {
      setchatRoom({ ...chatRoom, joined: false });
      
      socket.emit("leaveRoom", userRoom);
    } else {
      setchatRoom({ ...chatRoom, joined: true });
      
      socket.emit("joinRoom", userRoom);
    }
  };
  const renderChat = () => {
    // eslint-disable-next-line array-callback-return
    return chat.map(({ sender, message, room, time }, index) => {
      
      if (room === chatRoom.roomName && chatRoom.joined)
       {
        return (
          <div key={index} className="chat-messages">
            <div className="message">
              <p className="meta">
                {sender} <span> {time}</span>
              </p>
              <p className="text">{message}</p>
            </div>
          </div>
        );
      }
    });
  };

  const renderParticipants = () => {
    return (
      <div>
        {participants.map(function (p, idx) {
          return <li key={idx}>{p.username}</li>;
        })}
      </div>
    );
  };

  return (
    <div className="chat-component-div">
      <div className="tab-row">
      </div>
      <div className="chat-container">
        <main className="chat-main">
          <div className="chat-sidebar">
        <button
          className="profile-button"
          id="joinbtn"
          onClick={() => toggleRoomMembership(chatRoom.roomName)}
        >
        Join/Leave
        </button>
            <h2 id="room-name">
              <i className="fas fa-comments">
                <FontAwesomeIcon icon={faComment} />
              </i>
              {"\u00A0"}Board Name: {chatRoom.roomName}
            </h2>

            <h2 id="room-name">
              <i className="fas fa-users">
                <FontAwesomeIcon icon={faUser} />
              </i>
              {"\u00A0"}Users
            </h2>
            <ul id="users">{renderParticipants()}</ul>
            <div className="chat-messages-box">{renderChat()}</div>
          </div>
          <div className="chat-form-container">
            <form id="chat-form" onSubmit={onMessageSubmit}>
              <input
                name="message"
                type="text"
                onChange={(e) => onTextChange(e)}
                value={state.message}
                id="msg"
                variant="outlined"
                label="Message"
              />
              <button className="profile-button">
                <i className="fas fa-paper-plane">
                  <FontAwesomeIcon icon={faPaperPlane} />
                </i>
                Send
              </button>
            </form>
          </div>
        </main>
      </div>
    </div>
  );
};

export default Chat;
