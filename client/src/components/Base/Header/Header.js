import React from 'react';
import './Header.css';
import PropTypes from 'prop-types';

const Header =  ({ children }) => {

  return (
    <div className="Header">
      {children}
    </div>
  )
}

Header.propTypes = {
  children: PropTypes.object.isRequired,
};

export default Header;
