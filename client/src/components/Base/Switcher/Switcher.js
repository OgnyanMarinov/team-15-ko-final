import React, { useContext } from 'react';
import { IoMdMoon as Moon, IoMdSunny as Sun } from 'react-icons/io';

import ThemeContext from '../../../providers/ThemeContext';

export default function Switcher () {
  const { dark, toggle } = useContext(ThemeContext);
  
  return (
    <button 
      className='Switcher'
      onClick={() => toggle()}
    >
      <Sun className={`icon ${!dark ? 'active' : ''}`}/>
      <Moon className={`icon ${dark ? 'active' : ''}`}/>
    </button>
  );
} 