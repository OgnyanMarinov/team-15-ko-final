import React, { useContext, useState } from "react";
import "./Search.css";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import { BASE_URL } from "../../../common/constants";
import SearchContext from "../../../providers/SearchContext";

const Search = ({ history }) => {
  const searchContext = useContext(SearchContext);
  const [searchKeyword, setSearchKeyword] = useState("");

  const search = () => {
    if (!searchKeyword) {
      return;
    }

    fetch(`${BASE_URL}/search?keyword=${searchKeyword}`, {
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((r) => r.json())
      .then((result) => {
        // alert(result)
        if (result.error) {
          return alert(result.message);
        }
        searchContext.keyword = searchKeyword;
        searchContext.boards = result;
        searchContext.history.push(searchKeyword);

        history.push(`/search/results`);
      })
      .catch(error => alert(error.message));
  };

  const handleKeyDown = (e) => {
    if (!searchKeyword) {
      return;
    }
    if (e.key === "Enter") {
      e.preventDefault()
      search();
    }
  };

  return (
    <form id="demo-2">
      <input
        type="search"
        value={searchKeyword}
        onChange={(e) => setSearchKeyword(e.target.value)}
        onKeyDown={handleKeyDown}
        placeholder="Search"
      />
    </form>
  );
};

Search.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(Search);
