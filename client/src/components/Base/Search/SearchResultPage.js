import React, { useContext } from "react";
import SearchContext from "../../../providers/SearchContext";
import Card from "../../Pages/Boards/BoardsManagementPage/Card";
import moment from 'moment';

const SearchResultPage = () => {
  const searchContext = useContext(SearchContext);
console.log(searchContext)
  const renderCard = (board) => {
    return (
      <Card
        key={board.id}
        boardId={board.id}
        boardName={board.boardName}
        dateCreated={moment(board.dateCreated).format("lll")}
      />
    );
  };

  return (
    <>
      <h2 className="heading-public-private-boards">
        Search results with keyword: <u>{searchContext.keyword}</u>
      </h2>

      <div className="public-component-div">
        {searchContext.boards.map((board) => renderCard(board))}
      </div>
    </>
  );
};

export default SearchResultPage;
