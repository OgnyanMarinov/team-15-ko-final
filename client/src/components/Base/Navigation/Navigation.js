import React, { useContext } from "react";
import { Navbar, Nav, NavDropdown, Button, Col } from "react-bootstrap";
import "./Navigation.css";
import AuthContext from "../../../providers/AuthContext";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Switcher from "../Switcher/Switcher";
import Search from "../Search/Search";

const NavigationBar = ({ history }) => {
  const { isLoggedIn, user, setLoginState } = useContext(AuthContext);

  const logout = () => {
    setLoginState({
      isLoggedIn: false,
      user: null,
    });
    localStorage.removeItem("token");
    history.push("/home");
  };

  return (
    <Navbar collapseOnSelect expand="md" className="navbar-color" variant="dark">
    <Navbar.Brand href="/home">
        <img
          src="https://i.ibb.co/DbLW7D7/small-pencil-whiteboard.gif"
          width="150"
          height="58"
          className="d-inline-block align-top"
          alt="Logo"
          style={{borderRadius: "5px"}}
        />
      </Navbar.Brand>
      <Col xs={1}></Col>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link href="/home">Home</Nav.Link>
          <Nav.Link href="/boards">My Boards</Nav.Link>
          <Nav.Link href="/boards/public">Public Boards</Nav.Link>
        </Nav>
        <Nav>
          <Search />

          <Switcher />
          <NavDropdown alignRight title={<FontAwesomeIcon icon={faUser} />}>
            {isLoggedIn ? (
              <>
              <NavDropdown.Item href="/profile">Profile</NavDropdown.Item>
                <Navbar.Text className="singed-in">
                  Signed in as:
                  <NavDropdown.Item>
                    {user.username}
                  </NavDropdown.Item>
                </Navbar.Text>
                <Button
                  className="btn-logout"
                  variant="outline-info"
                  onClick={logout}
                >
                  Log out
                </Button>
                <br />
              </>
            ) : (
              <>
                <Col xs={1}></Col>
                <NavDropdown.Item href="/login">LogIn</NavDropdown.Item>
              </>
            )}
          </NavDropdown>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
};

NavigationBar.propTypes = {
  history: PropTypes.object.isRequired,
};
export default withRouter(NavigationBar);
