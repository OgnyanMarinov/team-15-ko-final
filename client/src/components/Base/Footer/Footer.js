import React from "react";
import "./Footer.css";

const Footer = () => {
  const currentDate = new Date();
  const logoData = {
    javaScript: {
      hyperlink: "https://developer.mozilla.org/en-US/docs/Web/JavaScript",
      logoUrl: "https://i.ibb.co/8MvXMVz/logo-1.png",
    },
    react: {
      hyperlink: "https://reactjs.org",
      logoUrl: "https://i.ibb.co/Gnv0M4h/logo-2.png",
    },
    nodeJS: {
      hyperlink: "https://nodejs.org/en/",
      logoUrl: "https://i.ibb.co/RgNNCJ2/logo-3.png",
    },
    nestJS: {
      hyperlink: "https://nestjs.com",
      logoUrl: "https://i.ibb.co/Xkq5614/logo-4.png",
    },
    mariaDB: {
      hyperlink: "https://mariadb.org",
      logoUrl: "https://i.ibb.co/8dBYyqx/logo-5.png",
    },
    jwt: {
      hyperlink: "https://jwt.io",
      logoUrl: "https://i.ibb.co/vVNBng2/logo-6.png",
    },
    konvaJS: {
      hyperlink: "https://konvajs.org",
      logoUrl: "https://i.ibb.co/svGqm37/logo-7.png",
    },
    socketIO: {
      hyperlink: "https://socket.io",
      logoUrl: "https://i.ibb.co/d2HkgV4/logo-8.png",
    },
  };

  const renderForm = () => {
    return Object.keys(logoData).map((key, index) => {
      return (
        <a key={index}
          target="_blank"
          rel="noopener noreferrer"
          href={logoData[key].hyperlink}
        >
          <img
            className="logo-footer"
            src={logoData[key].logoUrl}
            alt={logoData[key]}
          />
        </a>
      );
    });
  };

  return (
    <div className="footer-div">
      <p className="paragraph-footer">This App is powered by:</p>
      
      {renderForm()}
      
      <footer className="footer-footer">
        Copyright &copy; {currentDate.getFullYear()} KO's. All Rights Reserved
      </footer>
    </div>
  );
};

export default Footer;
