import React from "react";
import "./Footer.css";

const FooterPartial = () => {
  const currentDate = new Date();

  return (
    <div className="footer-div">
      <footer className="footer-footer">
        Copyright &copy; {currentDate.getFullYear()} KO's. All Rights Reserved
      </footer>
    </div>
  );
};

export default FooterPartial;
