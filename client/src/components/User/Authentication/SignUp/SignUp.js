import React, { useState } from "react";
import PropTypes from "prop-types";
import { BASE_URL } from "../../../../common/constants";
import AppError from "../../../Pages/AppError/AppError";
import "./SignUp.css";
import Header from "../../../Base/Header/Header";
import FooterPartial from "../../../Base/Footer/FooterPartial";

const SignUp = ({ history, location }) => {
  const [userReg, setUserReg] = useState({
    username: {
      value: "",
      touched: false,
      valid: false,
      type: "text",
      text: "Enter your username",
      placeholder: "Enter your username...",
    },
    email: {
      value: "",
      touched: false,
      valid: false,
      type: "email",
      text: "Enter your email",
      placeholder: "Enter your email...",
    },
    password: {
      value: "",
      touched: false,
      valid: false,
      type: "password",
      text: "Enter your password",
      placeholder: "Enter your password...",
    },
    passwordConfirm: {
      value: "",
      touched: false,
      valid: false,
      type: "password",
      text: "Enter your password again",
      placeholder: "Enter your password again...",
    },
  });
  const [error, setError] = useState(null);

  const getClassNames = (key) => {
    let classes = "input-empty ";

    if (userReg[key].touched) {
      classes += "touched ";
    }
    if (userReg[key].valid) {
      classes += "valid ";
    }
    if (userReg[key].touched && !userReg[key].valid) {
      classes += "invalid ";
    }

    return classes;
  };

  const validateForm = () =>
    !Object.keys(userReg).reduce(
      (isValid, key) => isValid && userReg[key].touched && userReg[key].valid,
      true
    );

  const validators = {
    username: [
      (value) => value && value.length >= 4, //|| `Username should be at least 4 characters!`,
      (value) => value && value.length <= 20, //|| `Username should be less than 20 characters!`,
    ],
    email: [
      // eslint-disable-next-line no-useless-escape
      (value) =>
        value && /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value), //|| `Write valid email structure!`
    ],
    password: [
      (value) => value && /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{5,}$/.test(value), //|| `Password should be 5 characters with letters and numbers!`,
    ],
    passwordConfirm: [(value) => value === userReg.password.value],
  };

  const updateTodoProp = (key, value) => {
    setUserReg({
      ...userReg,
      [key]: {
        value,
        touched: true,
        valid: validators[key].reduce(
          (isValid, validatorFn) => isValid && validatorFn(value),
          true
        ),
        type: userReg[key].type,
        text: userReg[key].text,
      },
    });
  };

  const signUp = () => {
    if (userReg.password.value !== userReg.passwordConfirm.value) {
      return alert(`Your passwords don't match!`);
    }
    if (!userReg.email.value) {
      return alert(`Please enter email address!`);
    }

    fetch(`${BASE_URL}/users`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username: userReg.username.value,
        email: userReg.email.value,
        password: userReg.password.value,
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }

        history.push("/login");
      })
      .catch((error) => setError(error.message));
  };

  const renderFormReg = () => {
    return Object.keys(userReg).map((key, index) => {
      return (
        <div key={index} className="input-container">
          <input
            type={userReg[key].type}
            id={`reg-${key}`}
            name={key}
            value={userReg[key].value}
            className={getClassNames(key)}
            onKeyDown={handleKeyDown}
            onChange={(e) => updateTodoProp(key, e.target.value)}
            // placeholder={user[key].placeholder}
          />
          <label htmlFor={`reg-${key}`} className="label-class">
            <span className="content-name">{userReg[key].text}</span>
          </label>
          <br />
          <br />
        </div>
      );
    });
  };

  const handleKeyDown = (e) => {
    if (
      userReg.password.valid === false ||
      userReg.passwordConfirm.valid === false
    ) {
      return;
    }
    if (e.key === "Enter") {
      signUp();
    }
  };

  return (
    <>
      <div className="div-form">
        {error ? <AppError message={error} /> : null}
        <Header>
          <h1>Register</h1>
        </Header>

        {renderFormReg()}

        <div className="button-container">
          <button
            id="reg-btn"
            className="profile-button"
            disabled={validateForm()}
            onClick={signUp}
          >
            Register
          </button>
        </div>
      </div>
      <FooterPartial />
    </>
  );
};

SignUp.propTypes = {
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
};

export default SignUp;
