import React, { useState } from "react";
import Header from "../../../Base/Header/Header";
import { BASE_URL } from "../../../../common/constants";
import AppError from "../../../Pages/AppError/AppError";
import PropTypes from "prop-types";
import FooterPartial from "../../../Base/Footer/FooterPartial";

const ResetPassword = ({ history }) => {
  const [email, setEmail] = useState("");
  const [error, setError] = useState(null);

  const validateForm = () => {
    // eslint-disable-next-line no-useless-escape
    const emailValid =
      email && /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email);

    return !emailValid;
  };

  const sendData = () => {
    fetch(`${BASE_URL}/mail/reset`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email,
      }),
    })
      //       .then(response => {
      // console.log(response)
      //         return response.json()
      //       })
      .then((result) => {
        console.log(result);
        if (result.error) {
          throw new Error(result.message);
        }

        history.push("/login");
      })
      .catch((error) => setError(error.message));
  };

  const handleKeyDown = (e) => {
    if (validateForm()) {
      return;
    }
    if (e.key === "Enter") {
      sendData();
    }
  };

  return (
    <>
      <div className="div-form">
        {error ? <AppError message={error} /> : null}
        <Header>
          <h1 className="reset">Reset your password</h1>
        </Header>

        <p style={{ padding: "10px" }}>
          Enter your user account's email address and we will send you a new
          password.
        </p>

        <input
          type="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          onKeyDown={handleKeyDown}
          placeholder="Enter your email..."
        />
        <br />
        <br />
        <button
          className="profile-button"
          onClick={sendData}
          disabled={validateForm()}
        >
          Send password reset email
        </button>
      </div>
      <FooterPartial />
    </>
  );
};

ResetPassword.propTypes = {
  history: PropTypes.object.isRequired,
};

export default ResetPassword;
