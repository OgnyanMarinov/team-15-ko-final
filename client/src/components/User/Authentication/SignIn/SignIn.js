import React, { useState, useContext, useEffect } from "react";
import PropTypes from "prop-types";
import { BASE_URL } from "../../../../common/constants";
import jwtDecode from "jwt-decode";
import AuthContext from "../../../../providers/AuthContext";
import AppError from "../../../Pages/AppError/AppError";
import "./SignIn.css";
import Header from "../../../Base/Header/Header";
import FooterPartial from "../../../Base/Footer/FooterPartial";

const SignIn = ({ history }) => {
  const [userLog, setUserLog] = useState({
    usernameOrEmail: {
      value: "",
      touched: false,
      valid: false,
      type: "text",
      text: "Enter your username",
      placeholder: "Enter your username...",
    },
    password: {
      value: "",
      touched: false,
      valid: false,
      type: "password",
      text: "Enter your password",
      placeholder: "Enter your password...",
    },
  });

  const [error, setError] = useState(null);
  const { isLoggedIn, setLoginState } = useContext(AuthContext);

  useEffect(() => {
    if (isLoggedIn) {
      history.push("/home");
    }
  }, [history, isLoggedIn]);

  const getClassNames = (key) => {
    let classes = "input-empty ";

    if (userLog[key].touched) {
      classes += "touched ";
    }
    if (userLog[key].valid) {
      classes += "valid ";
    }
    if (userLog[key].touched && !userLog[key].valid) {
      classes += "invalid ";
    }

    return classes;
  };

  const validateForm = () =>
    !Object.keys(userLog).reduce(
      (isValid, key) => isValid && userLog[key].touched && userLog[key].valid,
      true
    );

  const validators = {
    usernameOrEmail: [
      (value) => value && value.length >= 4, //|| `Username should be at least 4 characters!`,
      // value => value && value.length <= 20, //|| `Username should be less than 20 characters!`,
    ],
    password: [
      (value) => value && /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{5,}$/.test(value), //|| `Password should be 5 characters with letters and numbers!`,
    ],
  };

  const updateTodoProp = (key, value) => {
    setUserLog({
      ...userLog,
      [key]: {
        value,
        touched: true,
        valid: validators[key].reduce(
          (isValid, validatorFn) => isValid && validatorFn(value),
          true
        ),
        type: userLog[key].type,
        text: userLog[key].text,
      },
    });
  };

  const signIn = () => {
    if (!userLog.usernameOrEmail.value) {
      return alert(`Please enter username!`);
    }
    if (!userLog.password.value) {
      return alert(`Please enter password!`);
    }

    fetch(`${BASE_URL}/session`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        usernameOrEmail: userLog.usernameOrEmail.value,
        password: userLog.password.value,
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }

        try {
          const payload = jwtDecode(result.token);
          setLoginState({
            isLoggedIn: true,
            user: payload,
          });
        } catch (e) {
          return alert(e.message);
        }

        localStorage.setItem("token", result.token);
      })
      .catch((error) => setError(error.message));
  };

  const renderFormLog = () => {
    return Object.keys(userLog).map((key, index) => {
      return (
        <div key={index} className="input-container">
          <input
            type={userLog[key].type}
            id={`log-${key}`}
            name={key}
            value={userLog[key].value}
            className={getClassNames(key)}
            onKeyDown={handleKeyDown}
            onChange={(e) => updateTodoProp(key, e.target.value)}
            // placeholder={user[key].placeholder}
          />
          <label htmlFor={`log-${key}`} className="label-class">
            <span className="content-name">{userLog[key].text}</span>
          </label>
          <br />
          <br />
        </div>
      );
    });
  };

  const handleKeyDown = (e) => {
    if (userLog.password.valid === false) {
      return;
    }
    if (e.key === "Enter") {
      signIn();
    }
  };

  return (
    <>
      <div className="div-form">
        {error ? <AppError message={error} /> : null}
        <Header>
          <h1>LogIn</h1>
        </Header>

        {renderFormLog()}

        <div className="button-container">
          <button
            id="log-btn"
            className="profile-button"
            disabled={validateForm()}
            onClick={signIn}
          >
            Login
          </button>
        </div>
        <a href="/password_reset">I forgot my password</a>
        <p className="copyright-style">
          If you don't have an account please <a href="/register">Register</a>.
        </p>
      </div>
      <FooterPartial />
    </>
  );
};

SignIn.propTypes = {
  history: PropTypes.object.isRequired,
};

export default SignIn;
