/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect, useState, useContext } from "react";
import "./Profile.css";
import { BASE_URL, TOKEN } from "../../../../common/constants";
import AuthContext from "../../../../providers/AuthContext";
import AppError from "../../../Pages/AppError/AppError";
import DeleteUser from "../DeleteUserTab/DeleteUser";

const Profile = () => {
  const { user } = useContext(AuthContext);
  const [error, setError] = useState(null);
  const [preRenderInfo, setPreRenderInfo] = useState(false);
  const [preRenderPass, setPreRenderPass] = useState(false);
  const [sideButtons, setSideButtons] = useState({
    render: "profile",
  });
  const [confirmPassword, setConfirmPassword] = useState({
    currentPassword: {
      password: "",
      valid: false,
      touched: false,
    },
    newPassword: {
      password: "",
      valid: false,
      touched: false,
    },
    repeatNewPassword: {
      password: "",
      valid: false,
      touched: false,
    },
  });
  const [userData, setUserData] = useState({
    id: 0,
    username: "",
    firstName: "",
    lastName: "",
    displayName: "",
    userRole: 2,
    isDeleted: false,
    password: "",
    newPassword: "",
  });

  useEffect(() => {
    fetch(`${BASE_URL}/users/profile`, {
      headers: {
        Authorization: TOKEN,
        "Content-Type": "application/json",
      },
    })
      .then((resp) => resp.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setUserData({ ...result });
      })
      .catch((error) => setError(error.message));
  }, [user.id, preRenderInfo, preRenderPass]);

  const updateProfileInfo = () => {
    fetch(`${BASE_URL}/users/profile/edit-info`, {
      method: "POST",
      headers: {
        Authorization: TOKEN,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(userData),
    })
      .then((resp) => resp.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setPreRenderInfo(true);
      })
      .catch((error) => setError(error.message));
  };

  const updateProfilePassword = () => {
    if (
      confirmPassword.currentPassword.valid &&
      confirmPassword.newPassword.valid &&
      confirmPassword.repeatNewPassword.valid
    ) {
      userData.password = confirmPassword.currentPassword.password;
      userData.newPassword = confirmPassword.newPassword.password;
    } else {
      userData.password = "";
    }

    fetch(`${BASE_URL}/users/profile/edit-password`, {
      method: "POST",
      headers: {
        Authorization: TOKEN,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(userData),
    })
      .then((resp) => resp.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setConfirmPassword({
          ...confirmPassword,
          currentPassword: {
            password: "",
            valid: false,
            touched: false,
          },
          newPassword: {
            password: "",
            valid: false,
            touched: false,
          },
          repeatNewPassword: {
            password: "",
            valid: false,
            touched: false,
          },
        });
        setPreRenderPass(true);
        setError(null); // when successful to clear the error, because does not render the whole page
      })
      .catch((error) => setError(error.message));
  };

  const updateValues = (key, value) => {
    setUserData({ ...userData, [key]: value });
  };

  const getClassNames = (key) => {
    let classes = "input-profile ";

    if (confirmPassword[key].touched) {
      classes += "touched ";
    }
    if (confirmPassword[key].valid) {
      classes += "valid ";
    }
    if (confirmPassword[key] && !confirmPassword[key]) {
      classes += "invalid ";
    }

    return classes;
  };

  const validateForm = () =>
    !!!Object.keys(confirmPassword).reduce(
      (isValid, key) =>
        isValid && confirmPassword[key].touched && confirmPassword[key].valid,
      true
    );

  const validators = {
    currentPassword: [
      (value) =>
        (value && /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{5,}$/.test(value)) ||
        `Password should be 5 characters with one letters and numbers!`,
    ],
    newPassword: [
      (value) =>
        (value && /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{5,}$/.test(value)) ||
        `Password should be 5 characters with one letters and numbers!`,
    ],
    repeatNewPassword: [
      (value) =>
        value === confirmPassword.newPassword.password ||
        `Password does not match the new password!`,
    ],
  };

  const updatePassword = (key, value) => {
    setConfirmPassword({
      ...confirmPassword,
      [key]: {
        password: value,
        touched: true,
        valid: validators[key].reduce(
          (isValid, validatorFn) => isValid && validatorFn(value),
          true
        ),
      },
    });
  };

  const getValidationErrors = (key) => {
    return validators[key]
      .map((validatorFn) => validatorFn(confirmPassword[key].value))
      .filter((value) => typeof value === "string");
  };

  const renderValidationError = (key) =>
    confirmPassword[key].touched && !confirmPassword[key].valid
      ? getValidationErrors(key).map((error, index) => (
          <span className="error" key={index}>
            {error}
          </span>
        ))
      : null;

  return (
    <>
      {error ? <AppError message={error} /> : null}
      <h1 className="title-profile heading-homepage">
        Welcome to your profile page,
        <span className="title-span heading-homepage">
          <u>{userData.displayName || userData.username}</u>
        </span>
      </h1>
      <hr />

      <div className="div-form-profile">
        {sideButtons.render === "profile" ? (
          <>
            <div className="div-profile margin-div-profile">
              {/* <h3>Profile</h3> */}
              {preRenderInfo ? (
                <span>
                  Profile updated successfully
                  <br />
                </span>
              ) : null}
              <div className="input-container">
                <input
                  type="text"
                  id="input-firstName"
                  className="input-profile"
                  value={userData.firstName}
                  onChange={(e) => updateValues("firstName", e.target.value)}
                />
                <label htmlFor="input-firstName" className="label-class">
                  First name:{" "}
                </label>
                <br />
              </div>
              <div className="input-container-profile">
                <input
                  type="text"
                  id="input-lastName"
                  className="input-profile"
                  value={userData.lastName}
                  onChange={(e) => updateValues("lastName", e.target.value)}
                />
                <label htmlFor="input-lastName" className="label-class">
                  Last name:{" "}
                </label>
                <br />
              </div>
              {/* <hr /> */}
              <div className="input-container-profile">
                <input
                  type="text"
                  id="input-displayName"
                  className="input-profile"
                  value={userData.displayName}
                  onChange={(e) => updateValues("displayName", e.target.value)}
                />
                <label htmlFor="input-displayName" className="label-class">
                  Display name:{" "}
                </label>
                <br />
                <br />
              </div>

              <div className="input-container-profile">
                <button
                  className="profile-button height-important"
                  variant="info"
                  onClick={updateProfileInfo}
                >
                  Save Info
                </button>
              </div>
            </div>
            <div className="div-profile">
              {preRenderPass ? (
                <span>Successfully changed password!</span>
              ) : null}
              <form>
                <div className="input-container">
                  <input
                    type="password"
                    id="input-current-password"
                    className="input-profile"
                    value={confirmPassword.currentPassword.password}
                    onChange={(e) =>
                      updatePassword("currentPassword", e.target.value)
                    }
                  />
                  <br />
                  {renderValidationError("currentPassword")}
                  <br />
                  <label
                    htmlFor="input-current-password"
                    className="label-class"
                  >
                    Current password:{" "}
                  </label>
                </div>
                <div className="input-container-profile">
                  <input
                    type="password"
                    id="input-new-password"
                    className={getClassNames("newPassword")}
                    value={confirmPassword.newPassword.password}
                    onChange={(e) =>
                      updatePassword("newPassword", e.target.value)
                    }
                  />
                  <br />
                  {renderValidationError("newPassword")}
                  <br />
                  <label
                    htmlFor="input-container-profile"
                    className="label-class"
                  >
                    New password:{" "}
                  </label>
                </div>
                <div className="input-container-profile">
                  <input
                    type="password"
                    id="input-repeat-password"
                    className={getClassNames("repeatNewPassword")}
                    value={confirmPassword.repeatNewPassword.password}
                    onChange={(e) =>
                      updatePassword("repeatNewPassword", e.target.value)
                    }
                  />
                  <br />
                  {renderValidationError("repeatNewPassword")}
                  <label
                    htmlFor="input-repeat-password"
                    className="label-class"
                  >
                    Repeat new password:{" "}
                  </label>
                </div>
              </form>
              <div className="input-container-profile">
                <button
                  className="profile-button"
                  variant="info"
                  disabled={validateForm()}
                  onClick={updateProfilePassword}
                >
                  Change Password
                </button>
                <br />
              </div>
            </div>
            <div className="delete-div">
              <button
                className="profile-button delete-btn-profile"
                onClick={() =>
                  setSideButtons({ ...sideButtons, render: "delete-user" })
                }
              >
                Delete User
              </button>
            </div>
          </>
        ) : (
          <>
            {sideButtons.render === "delete-user" ? (
              <div className="div-profile-right">
                <h3>Delete User</h3>
                <DeleteUser />
              </div>
            ) : null}
          </>
        )}
      </div>
    </>
  );
};

export default Profile;
