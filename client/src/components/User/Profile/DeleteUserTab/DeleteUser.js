import React, { useContext, useState } from 'react';
import AuthContext from '../../../../providers/AuthContext';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { BASE_URL, TOKEN } from '../../../../common/constants';
import AppError from '../../../Pages/AppError/AppError';
import "./DeleteUser.css";

const DeleteUser = ({ history }) => {
  const { user, setLoginState } = useContext(AuthContext);
  const [error, setError] = useState(null);
  const [sureForm, setSureForm] = useState(false);

  const deleteLogout = () => {
    
      deleteUserFetch();
      setLoginState({
        isLoggedIn: false,
        user: null,
      });
      localStorage.removeItem('token');
      history.push('/home');
  };

  const deleteUserFetch = () => {
    console.log(`here`)
    fetch(`${BASE_URL}/users/${user.id}`, {
      method: 'DELETE',
      headers: {
        Authorization: TOKEN,
        'Content-Type': 'application/json',
      },
    })
      .then(resp => resp.json())
      .then(result => {
        if (result.error) {
          throw new Error(result.message);
        }
      })
      .catch(error => setError(error.message));
  };

  return (
    <div>
      {error ? <AppError message={error} /> : null}
      <p>If you want to delete your account, please press the <b>Delete Account</b> button.</p>
      {sureForm
        ? <div>
          <p>We are sorry to see you go! Are you sure you want to delete your account?</p>
          <button id='searchbtn' className="btn-log-reg button profile-button" type="button" onClick={deleteLogout}>YES</button>
          <button id='searchbtn' className="btn-log-reg button profile-button"  type="button" onClick={() => setSureForm(false)}>NO</button>
        </div>
        : <button
          id='searchbtn'
          className="btn-log-reg button profile-button"
          type="button"
          onClick={() => setSureForm(true)}>Delete Account
        </button>
      }
    </div>
  )
};

DeleteUser.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(DeleteUser);
