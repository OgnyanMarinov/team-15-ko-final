import React from "react";
import PropTypes from "prop-types";
import "./AppError.css";
import { Alert } from "react-bootstrap";

const AppError = ({ message }) => {
  return (
    <Alert className="error-page" variant="danger">
      <Alert.Heading>{message}</Alert.Heading>
    </Alert>

    // <div className="AppError">
    //   <h1>{message}</h1>
    // </div>
  );
};

AppError.propTypes = {
  message: PropTypes.string.isRequired,
};

export default AppError;
