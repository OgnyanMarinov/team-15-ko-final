import React from "react";
import Footer from "../../Base/Footer/Footer";

import "./HomeTest.css";
import whiteboard from "../../../data/Whiteboard.jpg";
import digital from "../../../data/Digital.jpg";
import { AiOutlineSecurityScan } from "react-icons/ai";
import { RiCodeLine, RiSettings4Line } from "react-icons/ri";

import { FaRegMoneyBillAlt} from "react-icons/fa";
const Home = () => {
  const cardsData = {
    draw: {
      name: "Drawing",
      imageClass: "card1",
      text:
        "With our App you will be able to draw your creativeness on the board. You can choose between a pencil and a brush with different thickness.",
    },
    text: {
      name: "Text",
      imageClass: "card2",
      text:
        "You want to leave a comment or share an idea... No problem at all just choose the textbox button and start typing. Text can become bold, italic and can even be transformed.",
    },
    publicPrivate: {
      name: "Public and Private",
      imageClass: "card3",
      text:
        "When creating boards, you can choose between private and public. With this option boards with sensitive information are kept hidden from curious eyes.",
    },
    share: {
      name: "Shareable boards",
      imageClass: "card4",
      text:
        "If you want to share your private board with the team members, that is not a problem with our App. All you need to do is write there email and send them a shareable link.",
    },
    chat: {
      name: "Realtime chat",
      imageClass: "card5",
      text: `Every board is 'equipped' with its own chat room. Therefore, team collaboration and brainstorming is possible in realtime.`,
    },
    pdf: {
      name: "Export to PDF",
      imageClass: "card6",
      text: "Boards can be exported as PDFs at anytime.",
    },
  };

  const renderForm = () => {
    return Object.keys(cardsData).map((key, index) => {
      return (
        <div key={index} className="card-home">
          <div className={`card-image ${cardsData[key].imageClass}`}></div>
          <div className="card-text">
            <h2>{cardsData[key].name}</h2>
            <p>{cardsData[key].text}</p>
          </div>
          <div className="card-stats"></div>
        </div>
      );
    });
  };

  return (
    <div>
      <div className="heading-div">
        <h1 className="heading-homepage">
          Welcome to our online whiteboard platform!
        </h1>
        <>
          <img src={whiteboard} alt="whiteboard" className="responsive" />
        </>
        <br />
        <br />
      </div>

      <div className="home-page-div">{renderForm()}</div>
      <br />
      <br />
      <img src={digital} alt="whiteboard" className="responsive" />
           <h1 className="heading-homepage">Why Choose Us</h1>
      <div className="cont">
        <div className="wrap">
          <div className="crd">
            <h3 className="title"><AiOutlineSecurityScan/> Privacy</h3>
           <p className='cardP'>We would not retain, sell or share any of your data</p>
          </div>
          <br />
          <br />
          <div className="crd">
            <h3 className="title"><FaRegMoneyBillAlt/> Free</h3>
            <p className='cardP'>Our service is absolutely free. Yes, you heard it right - No Charges</p>
          </div>
          <div className="crd">
            <h3 className="title"><RiCodeLine/> Software</h3>
            <p className='cardP'>We use open source. Everyone can see the code running the service</p>
          
          </div>
          <div className="crd">
            <h3 className="title"><RiSettings4Line/> Setup</h3>
            <p className='cardP'>Setup is easy. All you need is a modern browser</p>
          </div>
        </div>
      </div>
      <div className="callAction"><h1 className="heading-homepage" >Search no more!</h1>
      <a href="http://localhost:4000/register" className="profile-button large-btn">Register Now!</a></div>
      
      
      <Footer />
    </div>
  );
};

export default Home;
