import Konva from "konva";
import { historyArray } from "../../../../common/constants";

export const redoFunc = (layer) => {
  if (historyArray.length > 0) {
    const historyLength = historyArray.length;
    const layerLength = layer.children.length;

    if (historyLength > layerLength) {
      if (historyArray[layerLength] instanceof Konva.Text) {
        layer.children.push(
          historyArray[layerLength],
          historyArray[layerLength + 1]
        );
      } else {
        layer.children.push(historyArray[layerLength]);
      }
    }
    layer.draw();
  }
};
