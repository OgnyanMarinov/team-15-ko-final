export const zoomAndDrag = (stage, setZoom) => {
  stage.off("mousedown touchstart");
  stage.draggable(true);
  stage.container().style.cursor = "grab";

  stage.on("mousedown touchstart", (e) => {
    stage.container().style.cursor = "grabbing";
  });

  stage.on("mouseup touchend", (e) => {
    stage.container().style.cursor = "grab";
  });

  const scaleBy = 1.1;
  stage.on("wheel", (e) => {
    e.evt.preventDefault();
    const oldScale = stage.scaleX();

    const pointer = stage.getPointerPosition();

    const mousePointTo = {
      x: (pointer.x - stage.x()) / oldScale,
      y: (pointer.y - stage.y()) / oldScale,
    };

    const newScale = e.evt.deltaY > 0 ? oldScale * scaleBy : oldScale / scaleBy;

    stage.scale({ x: newScale, y: newScale });

    const zoomLevel = String(Math.round(newScale * 100));

    setZoom(zoomLevel);

    const newPos = {
      x: -(mousePointTo.x - pointer.x / newScale) * newScale,
      y: -(mousePointTo.y - pointer.y / newScale) * newScale,
    };
    stage.position(newPos);
    stage.batchDraw();
  });
};
