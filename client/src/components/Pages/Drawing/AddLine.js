import Konva from "konva";
import {
  brushCursor,
  eraserCursor,
  pencilCursor,
} from "../../../common/constants";

export const addLine = (stage, layer, color, mode, drawWidth) => {
  let isPaint = false;
  let lastLine;

  stage.draggable(false);
  stage.off("wheel");
  stage.off("mousedown touchstart");
  stage.off("mouseup touchend");

  const getRelativePointerPosition = (node) => {
    // the function will return pointer position relative to the passed node
    var transform = node.getAbsoluteTransform().copy();
    // to detect relative position we need to invert transform
    transform.invert();

    // get pointer (say mouse or touch) position
    var pos = node.getStage().getPointerPosition();

    // now we find relative point
    return transform.point(pos);
  };

  if (mode === "pencil") {
    stage.container().style.cursor = pencilCursor;
  }
  if (mode === "brush") {
    stage.container().style.cursor = brushCursor;
  }
  if (mode === "eraser") {
    stage.container().style.cursor = eraserCursor;
  }

  stage.on("mousedown touchstart", function (e) {
    isPaint = true;
    let pos = getRelativePointerPosition(stage);
    lastLine = new Konva.Line({
      stroke: `${color}`,
      strokeWidth: mode === "brush" ? +drawWidth : mode === "pencil" ? 1 : 10,
      globalCompositeOperation:
        mode === "eraser" ? "destination-out" : "source-over",
      points: [pos.x, pos.y],
      draggable: false,
    });
    layer.add(lastLine);
  });

  stage.on("mouseup touchend", function () {
    isPaint = false;
  });
  stage.on("mousemove touchmove", function () {
    if (!isPaint) {
      return;
    }

    const pos = getRelativePointerPosition(stage);
    let newPoints = lastLine.points().concat([pos.x, pos.y]);
    lastLine.points(newPoints);

    layer.batchDraw();
  });
};
