import React, { useState } from "react";
import { BASE_URL } from "../../../common/constants";
import { Button, Modal } from "react-bootstrap";

const InviteLink = ({id, setReloadStatus, setShowShare}) => {
  const [email, setEmail] = useState("");
  const boardId = id;

  const validateForm = () => {
    const emailValid =
    // eslint-disable-next-line no-useless-escape
      email && /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email);

    return !emailValid;
  };

  const sendData = () => {
    fetch(`${BASE_URL}/mail/invite/${boardId}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email,
      }),
    })
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setShowShare(false);
        setReloadStatus(Math.floor(Math.random() * 1e6));
      })
      .catch((error) => error.message);
  };

  const handleKeyDown = (e) => {
    if (validateForm()) {
      return;
    }
    if (e.key === "Enter") {
      sendData();
    }
  };

  return (
    <Modal.Dialog>
      <Modal.Header>
        <Modal.Title>Share board</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <span>Share with: </span><br />
        <input
        type="email"
        value={email}
        onChange={(e) => setEmail(e.target.value)}
        onKeyDown={handleKeyDown}
        placeholder="Enter  email..."
      />
      </Modal.Body>

      <Modal.Footer>
        <Button variant="primary" onClick={sendData} disabled={validateForm()}>Send invite link</Button>
        <Button variant="secondary" onClick={() => setShowShare(false)}>
          Close
        </Button>
      </Modal.Footer>
    </Modal.Dialog>
  );
};

export default InviteLink;
