import React from "react";
import AllUsersPublicBoards from "./AllUsersPublicBoards";
import FooterPartial from "../../../Base/Footer/FooterPartial";

const AllUsersPublicBoardsPage = () => {

  return (
    <div>
      <h2 className="heading-public-private-boards">These are all public boards!</h2><br />

      <AllUsersPublicBoards />
      <FooterPartial />
    </div>
  );
};

export default AllUsersPublicBoardsPage;
