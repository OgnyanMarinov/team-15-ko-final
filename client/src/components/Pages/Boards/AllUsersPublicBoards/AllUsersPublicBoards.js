import React, { useEffect, useState } from "react";
import { BASE_URL, TOKEN } from "../../../../common/constants";
import moment from 'moment';
import "../BoardPages.css";
import Card from "../BoardsManagementPage/Card";

const AllUsersPublicBoards = () => {
  const [boardData, setBoardData] = useState(null);
  const [reloadStatus, setReloadStatus] = useState(null);

  useEffect(() => {
    fetch(`${BASE_URL}/board/all-users-public`, {
      headers: {
        Authorization: TOKEN,
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setBoardData(result);
      })
      .catch((error) => console.log(`Error message: ` + error.message));
  }, [reloadStatus]);

  const renderCard = (board) => {

    return (
      <Card
        key={board.id}
        setReloadStatus={setReloadStatus}
        boardId={board.id}
        boardName={board.boardName}
        dateCreated={moment(board.dateCreated).format("lll")}
      />
    );
  };

  return (
    <div className="public-component-div">
      {boardData ? <>{boardData.map((k) => renderCard(k))}</> : null}
    </div>
  );
};

export default AllUsersPublicBoards;
