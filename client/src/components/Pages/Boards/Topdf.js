
import { jsPDF } from "jspdf";

export const ToPdf = (stageEl) => {
  var pdf = new jsPDF("l", "px", [stageEl.width(), stageEl.height()]);
  pdf.setTextColor("#000000");
  // first add texts
  stageEl.find("Text").forEach((text) => {
    const size = text.fontSize() / 0.75; // convert pixels to points
    pdf.setFontSize(size);
    pdf.text(text.text(), text.x(), text.y(), {
      baseline: "top",
      angle: -text.getAbsoluteRotation(),
      renderingMode: "invisible",
    });
  });

  // then put image on top of texts (so texts are not visible)
  pdf.addImage(
    stageEl.toDataURL({ pixelRatio: 2 }),
    0,
    0,
    stageEl.width(),
    stageEl.height()
  );

  pdf.save("canvas.pdf");
};

