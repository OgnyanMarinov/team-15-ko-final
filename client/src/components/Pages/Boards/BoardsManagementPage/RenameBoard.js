import React, { useState } from "react";
import { Button, Modal } from "react-bootstrap";
import { BASE_URL, TOKEN } from "../../../../common/constants";

const RenameBoard = ({ setReloadStatus, setShowRename, boardId, boardName }) => {
  const [name, setName] = useState(boardName);

  const renameBoard = () => {
    fetch(`${BASE_URL}/board/rename`, {
      method: "POST",
      headers: {
        Authorization: TOKEN,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id: boardId,
        boardName: name,
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setShowRename(false);
        setReloadStatus(Math.floor(Math.random() * 1e6));
      })
      .catch((error) => console.log(`Error message: ` + error.message));
  };

  const handleKeyDown = (e) => {
    if (name === "") {
      return;
    }
    if (e.key === "Enter") {
      renameBoard();
    }
    if (e.keyCode === 27) {
      setShowRename(false);
    }
  };

  return (
    <Modal.Dialog>
      <Modal.Header>
        <Modal.Title>Rename board</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <span>Enter a new board name:</span><br />
        <input
          id="rename-input"
          type="text"
          value={name}
          onChange={(e) => setName(e.target.value)}
          onKeyDown={handleKeyDown}
        />
      </Modal.Body>

      <Modal.Footer>
        <Button variant="primary" onClick={() => renameBoard()}>Save changes</Button>
        <Button variant="secondary" onClick={() => setShowRename(false)}>
          Close
        </Button>
      </Modal.Footer>
    </Modal.Dialog>
  );
};

export default RenameBoard;
