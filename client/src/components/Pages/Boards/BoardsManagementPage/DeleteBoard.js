import React from "react";
import { Button, Modal } from "react-bootstrap";
import { BASE_URL, TOKEN } from "../../../../common/constants";

const DeleteBoard = ({ setReloadStatus, setShowDelete, boardId, boardName }) => {

  const deleteBoardFetch = () => {
    fetch(`${BASE_URL}/board/delete`, {
      method: "POST",
      headers: {
        Authorization: TOKEN,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id: boardId,
        isDeleted: true,
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setShowDelete(false);
        setReloadStatus(Math.floor(Math.random() * 1e6));
      })
      .catch((error) => console.log(`Error message: ` + error.message));
  };

  return (
    <Modal.Dialog>
      <Modal.Body>
        <h2>Delete this board?</h2>
        <br />
        <span>
          You are about to delete <b>{boardName}</b>
        </span>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" onClick={() => deleteBoardFetch()}>
          Delete
        </Button>
        <Button variant="secondary" onClick={() => setShowDelete(false)}>
          Close
        </Button>
      </Modal.Footer>
    </Modal.Dialog>
  );
};

export default DeleteBoard;
