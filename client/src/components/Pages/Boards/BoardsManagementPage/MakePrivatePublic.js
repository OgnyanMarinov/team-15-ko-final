import React, { useEffect, useState } from "react";
import { Button, Modal } from "react-bootstrap";
import { BASE_URL, TOKEN } from "../../../../common/constants";

const MakePrivate = ({ setReloadStatus, setShowChangeStatus, boardId, boardName }) => {
  const [boardStatus, setBoardStatus] = useState(null);

  useEffect(() => {
    fetch(`${BASE_URL}/board/${boardId}`, {
      headers: {
        Authorization: TOKEN,
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setBoardStatus(result.isPrivate);
      })
      .catch((error) => console.log(`Error message: ` + error.message));
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const changeToPrivatePublic = () => {
    fetch(`${BASE_URL}/board/change-status`, {
      method: "POST",
      headers: {
        Authorization: TOKEN,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id: boardId,
        isPrivate: !boardStatus,
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        setShowChangeStatus(false);
        setReloadStatus(Math.floor(Math.random() * 1e6));
      })
      .catch((error) => console.log(`Error message: ` + error.message));
  };

  return (
    <Modal.Dialog>
      <Modal.Body>
        {boardStatus 
        ? <h2>Make board <u><b>{boardName}</b></u> public?</h2> 
        : <h2>Make board <u><b>{boardName}</b></u> private?</h2>}
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" onClick={() => changeToPrivatePublic()}>
        {boardStatus 
        ? `Make public` 
        : `Make private`}
        </Button>
        <Button variant="secondary" onClick={() => setShowChangeStatus(false)}>
          Close
        </Button>
      </Modal.Footer>
    </Modal.Dialog>
  );
};

export default MakePrivate;
