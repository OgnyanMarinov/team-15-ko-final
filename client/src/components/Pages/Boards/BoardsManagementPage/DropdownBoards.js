import React, { useState } from "react";
import { Dropdown, Modal } from "react-bootstrap";
import InviteLink from "../InviteLink";
import DeleteBoard from "./DeleteBoard";
import "./DropdownBoards.css";
import MakePrivatePublic from "./MakePrivatePublic";
import RenameBoard from "./RenameBoard";

const DropdownBoards = ({ setReloadStatus, boardId, boardName }) => {
  const [showRename, setShowRename] = useState(false);
  const [showDelete, setShowDelete] = useState(false);
  const [showMakePrivate, setShowChangeStatus] = useState(false);
  const [showShare, setShowShare] = useState(false);

  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    // eslint-disable-next-line jsx-a11y/anchor-is-valid
    <a
      className="anchor-dropdown"
      href=""
      ref={ref}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
    >
      {children}
      <span className="three-dots" />
    </a>
  ));

  return (
    <div className="dropdown-three-dots">
      <Dropdown drop="left">
        <Dropdown.Toggle as={CustomToggle} />
        <Dropdown.Menu size="sm" title="">
          <Dropdown.Item onClick={() => setShowRename(true)}>
            Rename board
          </Dropdown.Item>
          <Dropdown.Item onClick={() => setShowDelete(true)}>
            Delete board
          </Dropdown.Item>
          <Dropdown.Item onClick={() => setShowChangeStatus(true)}>
            Change board status
          </Dropdown.Item>
          <Dropdown.Item onClick={() => setShowShare(true)}>
            Share board
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>

      <Modal show={showRename}>
        <RenameBoard
          setReloadStatus={setReloadStatus}
          setShowRename={setShowRename}
          boardId={boardId}
          boardName={boardName}
        />
      </Modal>

      <Modal show={showDelete}>
        <DeleteBoard
          setReloadStatus={setReloadStatus}
          setShowDelete={setShowDelete}
          boardId={boardId}
          boardName={boardName}
        />
      </Modal>

      <Modal show={showMakePrivate}>
        <MakePrivatePublic
          setReloadStatus={setReloadStatus}
          setShowChangeStatus={setShowChangeStatus}
          boardId={boardId}
          boardName={boardName}
        />
      </Modal>

      <Modal show={showShare}>
        <InviteLink
          id={boardId}
          setReloadStatus={setReloadStatus}
          setShowShare={setShowShare}
        />
      </Modal>
    </div>
  );
};

export default DropdownBoards;
