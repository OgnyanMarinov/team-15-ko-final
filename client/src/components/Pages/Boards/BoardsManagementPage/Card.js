import React from 'react';
import DropdownBoards from './DropdownBoards';
import PropTypes from "prop-types";
import { withRouter } from 'react-router-dom';
import './Card.css';

const Card  = ({ history, setReloadStatus='', boardId, boardName, dateCreated }) => {

  return (
    <div className='Card'>
      <div className='img' onClick={() => history.push(`/boards/${boardId}`)}/>
      <DropdownBoards setReloadStatus={setReloadStatus} boardId={boardId} boardName={boardName} />
      <h4 className="card-board-title">{boardName}</h4>
      <p className="card-paragraph">Created: {dateCreated}</p>
    </div>
  );
}

Card.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(Card);