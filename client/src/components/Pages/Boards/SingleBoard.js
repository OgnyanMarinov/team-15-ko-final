import React, { useState, useEffect } from "react";
import { CompactPicker } from "react-color";
import { Stage, Layer } from "react-konva";
import { addLine } from "../Drawing/AddLine";
import { AddText } from "../Drawing/AddText";
import { redoFunc } from "../Drawing/UndoAndRedo/RedoFunc";
import { undoFunc } from "../Drawing/UndoAndRedo/UndoFunc";
import { zoomAndDrag } from "../Drawing/ZoomAndDrag";
import { loadBoard } from "./CreateSaveLoad/LoadBoard";
import { saveBoard } from "./CreateSaveLoad/SaveBoard";
import { ToPdf } from "./ToPdf";
import Chat from "../../Base/Chat/Chat";
import PropTypes from "prop-types";
import { HiPencil } from "react-icons/hi";
import { BsBrush } from "react-icons/bs";
import { BiText } from "react-icons/bi";
import {
  FaEraser,
  FaItalic,
  FaBold,
  FaHandPaper,
  FaSave,
} from "react-icons/fa";
import { MdColorLens } from "react-icons/md";
import { AiOutlineFilePdf } from "react-icons/ai";
import { ImUndo2, ImRedo2 } from "react-icons/im";
import FooterPartial from "../../Base/Footer/FooterPartial";
import AppError from "../AppError/AppError";

const SingleBoard = ({ match, history }) => {
  const stageEl = React.createRef();
  const layerEl = React.createRef();
  const [color, setColor] = useState("black");
  const [drawState, setDrawState] = useState("pencil");
  const [showColorPicker, setShowColorPicker] = useState(false);
  const [zoom, setZoom] = useState(`100`);
  const [drawWidth, setDrawWidth] = useState("2");
  const boardId = match.params.id;
  const [error, setError] = useState(null);
  useEffect(() => {
    addLine(stageEl.current.getStage(), layerEl.current, color, drawState);
    setShowColorPicker(false);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [color]);

  useEffect(() => {
    loadBoard(
      layerEl.current,
      stageEl.current.getStage(),
      +match.params.id,
      setError
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [stageEl.current]);

  const handleBrushThickness = (e) => {
    setDrawWidth(e.currentTarget.value);
    addLine(
      stageEl.current.getStage(),
      layerEl.current,
      color,
      "brush",
      drawWidth
    );
  };

  return (
    <>
      {error ? <AppError message={error} /> 
      :
    <div className="singleboard-page-div">
      <Chat Id={boardId} />
      <div className="toolbox">
        <button
          className="profile-button"
          title="Export PDF"
          style={{ float: "left" }}
          onClick={() => ToPdf(stageEl.current)}
        >
          <AiOutlineFilePdf size={20} />
        </button>

        <button
          className="profile-button space-margin"
          style={{ float: "left" }}
          onClick={() => saveBoard(layerEl.current, +match.params.id)}
        >
          <FaSave size={20} />
        </button>

        <button
          className="profile-button"
          style={{ float: "left" }}
          onClick={() => undoFunc(layerEl.current)}
        >
          <ImUndo2 size={20} />
        </button>

        <button
          className="profile-button space-margin"
          style={{ float: "left" }}
          onClick={() => redoFunc(layerEl.current)}
        >
          <ImRedo2 size={20} />
        </button>

        {/* <button
          className="profile-button"
          style={{ float: "left" }}
          onClick={() => loadBoard(layerEl.current, stageEl.current.getStage())}
        >
          <HiFolder /> Load
        </button> */}

        <button
          className="profile-button"
          style={{ float: "left" }}
          onClick={() => {
            addLine(
              stageEl.current.getStage(),
              layerEl.current,
              color,
              "pencil"
            );
            setDrawState("pencil");
          }}
        >
          <HiPencil size={20} />
        </button>

        <button
          className="profile-button"
          style={{ float: "left" }}
          onClick={() => {
            addLine(
              stageEl.current.getStage(),
              layerEl.current,
              color,
              "brush",
              drawWidth
            );
            setDrawState("brush");
          }}
        >
          <BsBrush size={20} />
        </button>

        <button
          className="profile-button space-margin"
          style={{ float: "left" }}
          onClick={() => {
            addLine(
              stageEl.current.getStage(),
              layerEl.current,
              color,
              "eraser"
            );
            setDrawState("eraser");
          }}
        >
          <FaEraser size={20} />
        </button>

        {showColorPicker ? (
          <>
            <CompactPicker
              color={color}
              onChange={(color) => setColor(color.hex)}
            />
          </>
        ) : (
          <button
            className="profile-button space-margin"
            onClick={() => setShowColorPicker(true)}
          >
            <MdColorLens size={20} />
          </button>
        )}

        <button
          className="profile-button"
          style={{ float: "left" }}
          onClick={() =>
            AddText(stageEl.current.getStage(), layerEl.current, color)
          }
        >
          <BiText size={21} />
        </button>

        <button
          id="italic-btn"
          style={{ float: "left" }}
          className="profile-button"
        >
          <FaItalic size={20} />
        </button>

        <button
          id="bold-btn"
          style={{ float: "left" }}
          className="profile-button space-margin"
        >
          <FaBold size={20} />
        </button>

        <button
          className="profile-button"
          style={{ float: "left" }}
          onClick={() => zoomAndDrag(stageEl.current.getStage(), setZoom)}
        >
          <FaHandPaper size={20} />
        </button>

        <br />
        <br />
        <span className="zoom-span">{`Zoom: ${zoom}%`}</span>
        <span className="brush-width-span">Brush width: </span>
        <input
          type="range"
          id="thickness"
          name="thickness"
          min="2"
          max="30"
          step="1"
          value={drawWidth}
          onChange={handleBrushThickness}
        />
        <span className="zoom-span">{drawWidth}</span>
      </div>
      <div className="stage-div">
        <Stage width={1000} height={500} ref={stageEl}>
          <Layer ref={layerEl}>
            {/* <Transformer
            nodes={[]}
            boundBoxFunc={function (oldBox, newBox) {
              newBox.width = Math.max(30, newBox.width);
              return newBox;
            }}
          ></Transformer> */}
          </Layer>
        </Stage>
      </div>
      <FooterPartial />
    </div>
        }
        </>
  );
};

SingleBoard.propTypes = {
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default SingleBoard;
