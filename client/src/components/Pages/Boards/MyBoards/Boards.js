import React, { useState } from "react";
import PublicBoards from "./PublicBoards";
import PrivateBoards from "./PrivateBoards";
import FooterPartial from "../../../Base/Footer/FooterPartial";
import '../BoardPages.css';
import CreateBoard from "../CreateSaveLoad/CreateBoard";

const Boards = () => {
  const [createForm, setCreateForm] = useState(false);
  const [showBoards, setShowBoards] = useState(true);

  return (
    <div>
      {showBoards ? (
        <h2 className="heading-public-private-boards">
          These are your public boards!
        </h2>
      ) : (
        <h2 className="heading-public-private-boards">
          These are your private boards!
        </h2>
      )}

      <div className="div-btn-board">
        <button
          className="profile-button"
          onClick={() => setCreateForm(!createForm)}
        >
          Create Board
        </button>

        {showBoards ? (
          <button
            className="profile-button margin-btn"
            onClick={() => {
              setShowBoards(!showBoards);
              setCreateForm(false);
            }}
          >
            Show Private Boards
          </button>
        ) : (
          <button
            className="profile-button margin-btn"
            onClick={() => {
              setShowBoards(!showBoards);
              setCreateForm(false);
            }}
          >
            Show Public Boards
          </button>
        )}
      </div>

      {createForm ? <CreateBoard /> : null}

      {showBoards ? <PublicBoards /> : <PrivateBoards />}

      <FooterPartial />
    </div>
  );
};

export default Boards;
