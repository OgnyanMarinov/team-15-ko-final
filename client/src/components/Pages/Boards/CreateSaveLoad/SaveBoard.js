import { BASE_URL, TOKEN } from "../../../../common/constants";
import Konva from "konva";
export const saveBoard = (layer, boardId) => {
  const lineData = {
    points: [],
    stroke: [],
    strokeWidth: [],
    globalCompositeOperation: [],
  };

  const textData = {
    text: [],
    x: [],
    y: [],
    fontSize: [],
    fontStyle: [],
    width: [],
    id: [],
    fill: [],
  };

  // eslint-disable-next-line array-callback-return
  layer.children.map((key, index) => {
    if (layer.children[index] instanceof Konva.Text) {
      textData.text.push(key.attrs.text);
      textData.x.push(key.attrs.x);
      textData.y.push(key.attrs.y);
      textData.fontSize.push(key.attrs.fontSize);
      textData.fontStyle.push(key.attrs.fontStyle);
      textData.width.push(key.attrs.width);
      textData.id.push(key.attrs.id);
      textData.fill.push(key.attrs.fill);
    } else if (layer.children[index] instanceof Konva.Line) {
      lineData.points.push(key.attrs.points);
      lineData.stroke.push(key.attrs.stroke);
      lineData.strokeWidth.push(key.attrs.strokeWidth);
      lineData.globalCompositeOperation.push(
        key.attrs.globalCompositeOperation
      );
    }
  });

  // const wholeObject = {
  //   text: [],
  //   transform: [],
  // };
  // console.log(layer.children[1] instanceof Konva.Text);

  // wholeObject.text.push(layer.children[0]);
  // wholeObject.transform.push(layer.children[1]);

  console.log(lineData);
  console.log(textData);

  fetch(`${BASE_URL}/board/save`, {
    method: "POST",
    headers: {
      Authorization: TOKEN,
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      id: boardId,
      linesData: JSON.stringify(lineData),
      textsData: JSON.stringify(textData),
    }),
  })
    .then((response) => response.json())
    .then((result) => {
      if (result.error) {
        throw new Error(result.message);
      }
    })
    .catch((error) => console.log(`Error message: ` + error.message));
};
