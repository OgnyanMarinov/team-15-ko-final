import React, { useState } from "react";
import { withRouter } from "react-router-dom";
import { BASE_URL, TOKEN } from "../../../../common/constants";
import PropTypes from 'prop-types';

const CreateBoard = ({ history }) => {
  const [boardName, setBoardName] = useState(null);

  const createFetch = (boardName, privateBoardStatus) => {
    fetch(`${BASE_URL}/board/create`, {
      method: "POST",
      headers: {
        Authorization: TOKEN,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        boardName: boardName,
        isPrivate: privateBoardStatus,
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.error) {
          throw new Error(result.message);
        }
        history.push(`/boards/${result.id}`);
      })
      .catch((error) => console.log(`Error message: ` + error.message));
  };

  return (
    <div className="div-create">
      <span className="span-board">Board Name: </span>
      <input
        type={`text`}
        id={`create-form`}
        value={boardName}
        onChange={(e) => setBoardName(e.target.value)}
        placeholder={`Enter your board's name`}
      />

      <button
        className="profile-button margin-btn"
        onClick={() => createFetch(boardName, true)}
      >
        Create Private
      </button>
      <button
        className="profile-button margin-btn"
        onClick={() => createFetch(boardName, false)}
      >
        Create Public
      </button>
    </div>
  );
};

CreateBoard.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(CreateBoard);
