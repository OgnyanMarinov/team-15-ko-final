import { BASE_URL, TOKEN } from "../../../../common/constants";
import Konva from "konva";

export const loadBoard = (layer, stage, boardId, setError) => {
  fetch(`${BASE_URL}/board/load`, {
    method: "POST",
    headers: {
      Authorization: TOKEN,
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      id: boardId,
    }),
  })
    .then((response) => response.json())
    .then((result) => {
      if (result.error) {
        throw new Error(result.message);
      }
      const data = result;
      data.linesData = JSON.parse(data.linesData);
      data.textsData = JSON.parse(data.textsData);
      
      // console.log(data);
      // console.log(JSON.parse(data.linesData.transform[0]));
      // console.log(data.linesData.points.length)
      // console.log(data.textsData.text.length)
      if (data.linesData !== null) {
      for (let k = 0; k < data.linesData.points.length; k++) {
        layer.add(
          new Konva.Line({
            stroke: data.linesData.stroke[k],
            strokeWidth: data.linesData.strokeWidth[k],
            globalCompositeOperation:
              data.linesData.globalCompositeOperation[k],
            points: data.linesData.points[k],
            draggable: false,
          })
        );
      }
    }
    if (data.textsData !== null) {
      for (let k = 0; k < data.textsData.text.length; k++) {
        const createKonvaText = new Konva.Text({
          text: data.textsData.text[k],
          x: data.textsData.x[k],
          y: data.textsData.y[k],
          fontSize: data.textsData.fontSize[k],
          fontStyle: data.textsData.fontStyle[k],
          draggable: true,
          width: data.textsData.width[k],
          id: data.textsData.id[k],
          fill: data.textsData.fill[k],
        });
        layer.add(createKonvaText);

        const createKonvaTransformer = new Konva.Transformer({
          nodes: [createKonvaText],
          boundBoxFunc: function (oldBox, newBox) {
            newBox.width = Math.max(30, newBox.width);
            return newBox;
          },
        });

        const container = stage.container();
        container.tabIndex = 1;
        container.focus();
        let lastShape = null;

        stage.on("click", function (e) {
          if (!this.clickStartShape) {
            return;
          }
          if (e.target !== stage) {
            lastShape = e.target;
          }

          container.addEventListener("keydown", (del) => {
            del.preventDefault();
            if (del.keyCode === 46 && lastShape !== null) {
              layer.children.splice(lastShape.index, 1);
              lastShape.destroy();
              layer.draw();
            }
          });

          document
            .getElementById("italic-btn")
            .addEventListener("click", () => {
              // eslint-disable-next-line array-callback-return
              layer.children.filter((text) => {
                if (text._id === this.clickStartShape._id) {
                  text.attrs.fontStyle === "normal"
                    ? (text.attrs.fontStyle = "italic")
                    : text.attrs.fontStyle === "bold"
                    ? (text.attrs.fontStyle = "italic bold")
                    : text.attrs.fontStyle === "italic"
                    ? (text.attrs.fontStyle = "normal")
                    : text.attrs.fontStyle === "italic bold"
                    ? (text.attrs.fontStyle = "bold")
                    : (text.attrs.fontStyle = "normal");
                }
              });
              layer.draw();
            });
          document.getElementById("bold-btn").addEventListener("click", () => {
            // eslint-disable-next-line array-callback-return
            layer.children.filter((text) => {
              if (text._id === this.clickStartShape._id) {
                console.log(text.attrs.fontStyle);
                text.attrs.fontStyle === "normal"
                  ? (text.attrs.fontStyle = "bold")
                  : text.attrs.fontStyle === "italic"
                  ? (text.attrs.fontStyle = "italic bold")
                  : text.attrs.fontStyle === "bold"
                  ? (text.attrs.fontStyle = "normal")
                  : text.attrs.fontStyle === "italic bold"
                  ? (text.attrs.fontStyle = "italic")
                  : (text.attrs.fontStyle = "normal");
              }
              layer.draw();
            });
          });
          if (e.target._id === this.clickStartShape._id) {
            layer.add(createKonvaTransformer);
            createKonvaTransformer.nodes([e.target]);
            layer.draw();
          } else {
            createKonvaTransformer.detach();
            layer.draw();
          }
        });

        createKonvaText.on("transform", function () {
          // reset scale, so only with is changing by transformer
          createKonvaText.setAttrs({
            width: createKonvaText.width() * createKonvaText.scaleX(),
            scaleX: 1,
          });
        });
        layer.add(createKonvaTransformer);
        layer.draw();
        createKonvaText.on("dblclick", () => {
          createKonvaText.hide();
          createKonvaTransformer.hide();
          layer.draw();

          let textPosition = createKonvaText.absolutePosition();
          let stageBox = stage.container().getBoundingClientRect();
          let areaPosition = {
            x: textPosition.x + stageBox.left,
            y: textPosition.y + stageBox.top,
          };
          let textarea = document.createElement("textarea");
          document.body.appendChild(textarea);
          textarea.value = createKonvaText.text();
          textarea.style.position = "absolute";
          textarea.style.top = areaPosition.y + "px";
          textarea.style.left = areaPosition.x + "px";
          textarea.style.width =
            createKonvaText.width() - createKonvaText.padding() * 2 + "px";
          textarea.style.height =
            createKonvaText.height() - createKonvaText.padding() * 2 + 5 + "px";
          textarea.style.fontSize = createKonvaText.fontSize() + "px";
          textarea.style.border = "none";
          textarea.style.padding = "0px";
          textarea.style.margin = "0px";
          textarea.style.overflow = "hidden";
          textarea.style.background = "none";
          textarea.style.outline = "none";
          textarea.style.resize = "none";
          textarea.style.lineHeight = createKonvaText.lineHeight();
          textarea.style.fontFamily = createKonvaText.fontFamily();
          textarea.style.transformOrigin = "left top";
          textarea.style.textAlign = createKonvaText.align();
          textarea.style.color = createKonvaText.fill();
          let rotation = createKonvaText.rotation();
          let transform = "";
          if (rotation) {
            transform += "rotateZ(" + rotation + "deg)";
          }
          let px = 0;
          let isFirefox =
            navigator.userAgent.toLowerCase().indexOf("firefox") > -1;
          if (isFirefox) {
            px += 2 + Math.round(createKonvaText.fontSize() / 20);
          }
          transform += "translateY(-" + px + "px)";
          textarea.style.transform = transform;
          textarea.style.height = "auto";
          textarea.style.height = textarea.scrollHeight + 3 + "px";
          textarea.focus();

          function removeTextarea() {
            textarea.parentNode.removeChild(textarea);
            window.removeEventListener("click", handleOutsideClick);
            createKonvaText.show();
            createKonvaTransformer.show();
            createKonvaTransformer.forceUpdate();
            layer.draw();
          }
          function setTextareaWidth(newWidth) {
            if (!newWidth) {
              newWidth =
                createKonvaText.placeholder.length * createKonvaText.fontSize();
            }
            let isSafari = /^((?!chrome|android).)*safari/i.test(
              navigator.userAgent
            );
            let isFirefox =
              navigator.userAgent.toLowerCase().indexOf("firefox") > -1;
            if (isSafari || isFirefox) {
              newWidth = Math.ceil(newWidth);
            }
            let isEdge =
              document.documentMode || /Edge/.test(navigator.userAgent);
            if (isEdge) {
              newWidth += 1;
            }
            textarea.style.width = newWidth + "px";
          }
          textarea.addEventListener("keydown", function (e) {
            if (e.keyCode === 13 && !e.shiftKey) {
              createKonvaText.text(textarea.value);
              removeTextarea();
            }
            if (e.keyCode === 27) {
              removeTextarea();
            }
          });
          textarea.addEventListener("keydown", function () {
            let scale = createKonvaText.getAbsoluteScale().x;
            setTextareaWidth(createKonvaText.width() * scale);
            textarea.style.height = "auto";
            textarea.style.height =
              textarea.scrollHeight + createKonvaText.fontSize() + "px";
          });
          function handleOutsideClick(e) {
            if (e.target !== textarea) {
              createKonvaText.text(textarea.value);
              removeTextarea();
            }
          }
          setTimeout(() => {
            window.addEventListener("click", handleOutsideClick);
          });
        });
        createKonvaTransformer.detach();
      }
    }
      layer.draw();
    })
    .catch((error) => setError(error.message));
};
