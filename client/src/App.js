import React, { useState } from 'react';
import './App.css';
import { BrowserRouter, Switch, Redirect, Route } from "react-router-dom";
import NotFound from './components/Pages/NotFound/NotFound';
import NavigationBar from './components/Base/Navigation/Navigation';
import AuthContext, { extractUser, getToken } from './providers/AuthContext';
import Profile from './components/User/Profile/ProfileTab/Profile';
import GuardedRoute from './providers/GuardedRoute';
import Home from './components/Pages/Home/Home';
import SignIn from './components/User/Authentication/SignIn/SignIn';
import SignUp from './components/User/Authentication/SignUp/SignUp';
import ResetPassword from './components/User/Authentication/ResetPassword/ResetPassword';
import Boards from './components/Pages/Boards/MyBoards/Boards';
import SingleBoard from './components/Pages/Boards/SingleBoard';
import AllUsersPublicBoardsPage from './components/Pages/Boards/AllUsersPublicBoards/AllUsersPublicBoardsPage';
import SearchResultPage from './components/Base/Search/SearchResultPage';

const App = () => {

  const [authValue, setAuthValue] = useState({
    isLoggedIn: !!extractUser(getToken()),
    user: extractUser(getToken()),
  });

  return (
    <div className="div-background">
      <BrowserRouter>
        <AuthContext.Provider value={{ ...authValue, setLoginState: setAuthValue }}>
          <NavigationBar />
          <div className='div-inside'>
            <Switch>
              <Redirect path="/" exact to="/home" />
              <Route path="/login" component={SignIn} />
              <Route path="/register" component={SignUp} />
              <Route path="/password_reset" component={ResetPassword} />
              <GuardedRoute path="/profile" auth={authValue.isLoggedIn} component={Profile} />
              <Route path="/home" component={Home} />
              <GuardedRoute path="/boards/public" auth={authValue.isLoggedIn} component={AllUsersPublicBoardsPage} />
              <GuardedRoute path="/boards/:id" auth={authValue.isLoggedIn} component={SingleBoard} />
              <GuardedRoute path="/boards" auth={authValue.isLoggedIn} component={Boards} />
              <Route path="/search/results" component={SearchResultPage} />
              <Route path="*" component={NotFound} />
            </Switch>
          </div>
        </AuthContext.Provider>      
      </BrowserRouter>
    </div>
  );
};

export default App;
