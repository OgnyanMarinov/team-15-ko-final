import React, { useState, useLayoutEffect } from 'react';

const ThemeContext = React.createContext({
  dark: false,
  toggle: () => {},
});

export default ThemeContext;

export function ThemeProvider (props) {
  // keeps state of the current theme
  const [dark, setDark] = useState(false);
  
  // paints the app before it renders elements
  useLayoutEffect(() => {
    const lastTheme = window.localStorage.getItem('darkTheme');
    
    if (lastTheme === 'true') {
      setDark(true);
      applyTheme(darkTheme);
    } else {
      setDark(false);
      applyTheme(lightTheme);
    } 
  // if state changes, repaints the app
  }, [dark]);

  // rewrites set of css variablels/colors
  const applyTheme = theme => {
    const root = document.getElementsByTagName('html')[0];
    root.style.cssText = theme.join(';');
  }

  const toggle = () => {
    const body = document.getElementsByTagName('body')[0];
    body.style.cssText = 'transition: background .5s ease';

    setDark(!dark);
    window.localStorage.setItem('darkTheme', !dark);
  };

  return (
    <ThemeContext.Provider value={{
      dark,
      toggle,
    }}>
      {props.children}
    </ThemeContext.Provider>
  )
}


// styles
const lightTheme = [
  '--border: rgba(0,0,0,.2)',
  '--shadow: #000',
  '--heading: rgba(255,100,0,1)',
  '--main: #1d8f13',
  '--text: #000',
  '--textAlt: #fff',
  '--inactive: rgba(0,0,0,.3)',
  '--background: white',
  '--chat-heading: white',
  '--dark-color-a: #24305e',
	'--dark-color-b: #374785',
	'--light-color: #a8d0e6',
	'--success-color: #5cb85c',
  '--error-color: #d9534f',
  '--chat-text: 255,255,255',
  '--button-shadow:#7a8eb9',
  '--linear-gradient-bottom: #24305e',
  '--linear-gradient: #374685',
  '--btn-border: #343d42',
  '--toolbox-background: #eee',
  '--card-background: #637aad',
  '--gradient-back: rgba(255,255,255,1)',
  '--gradient-8: rgba(236,240,243,1)',
  '--gradient-100:  rgba(196,209,217,1)',
  '--card-background: #637aad',
  '--board-card: #ffffff',
  
  
];

const darkTheme = [
  '--border: rgba(255,255,255,.1)',
  '--shadow: #000',
  '--heading: rgba(255,255,5,.9)',
  '--main: #79248f',
  '--text: rgb(255, 255, 255)',
  '--textAlt: #fff',
  '--inactive: rgba(255,255,255,.3)',
  '--background: #2D2D2D',
  '--chat-heading: #637aad',
  '--dark-color-a: #1f2833',
	'--dark-color-b: #495057 ',
	'--light-color: #a8d0e6',
	'--success-color: #5cb85c',
  '--error-color: #d9534f',
  '--button-shadow:#7a8eb9',
  '--linear-gradient-bottom: #637aad',
  '--linear-gradient: ##5972a7',
  '--btn-border: #314179',
  '--toolbox-background: #495057',
  '--card-background: #24305e',
  '--gradient-back: rgba(55,64,97,1)',
  '--gradient-8: rgba(53,59,87,1)',
  '--gradient-100: rgba(54,57,122,0.99)',
  '--card-background: #24305e',
  '--board-card: #7a8eb9',
 

];
