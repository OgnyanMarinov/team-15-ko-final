import { createContext } from 'react';

const SearchContext = createContext({
  keyword: '',
  boards: [],
  history: [],
});

export default SearchContext;
